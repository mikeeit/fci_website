﻿//$(document).ready(function () {
    $("#ngaybd").datepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#ngaykt').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#ngaykt').datepicker('setStartDate', null);
    });
    $("#ngaykt").datepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('#ngaybd').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('#ngaybd').datepicker('setEndDate', null);
    });
    $('.timepicker').timepicker({
        showInputs: false,
        showMeridian: false
    });
//Thu hai
    $("#thuhaitp1").on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        $('#thuhaitp2').timepicker('setTime', (hour + 1) + ':' + e.time.minutes);
        if (hour < 7) {
            $('#thuhaitp1').timepicker('setTime', '7:' + e.time.minutes + ' AM');
        }
        else if (hour > 18) {
            $('#thuhaitp1').timepicker('setTime', '18:' + '00' + ' PM');
        }
    });

    $('#thuhaitp2').on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        if (hour < 8) {
            $('#thuhaitp2').timepicker('setTime', '8:' + e.time.minutes + ' AM');
        }
        else if (hour > 21) {
            $('#thuhaitp2').timepicker('setTime', '21:' + '00' + ' PM');
        }
    });
    //Thu ba
    $("#thubatp1").on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        $('#thubatp2').timepicker('setTime', (hour + 1) + ':' + e.time.minutes);
        if (hour < 7) {
            $('#thubatp1').timepicker('setTime', '7:' + e.time.minutes + ' AM');
        }
        else if (hour > 18) {
            $('#thubatp1').timepicker('setTime', '18:' + '00' + ' PM');
        }
    });

    $('#thubatp2').on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        if (hour < 8) {
            $('#thubatp2').timepicker('setTime', '8:' + e.time.minutes + ' AM');
        }
        else if (hour > 21) {
            $('#thubatp2').timepicker('setTime', '21:' + '00' + ' PM');
        }
    });
    //Thu tu
    $("#thututp1").on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        $('#thututp2').timepicker('setTime', (hour + 1) + ':' + e.time.minutes);
        if (hour < 7) {
            $('#thututp1').timepicker('setTime', '7:' + e.time.minutes + ' AM');
        }
        else if (hour > 18) {
            $('#thututp1').timepicker('setTime', '18:' + '00' + ' PM');
        }
    });

    $('#thututp2').on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        if (hour < 8) {
            $('#thututp2').timepicker('setTime', '8:' + e.time.minutes + ' AM');
        }
        else if (hour > 21) {
            $('#thututp2').timepicker('setTime', '21:' + '00' + ' PM');
        }
    });
    //Thu nam
    $("#thunamtp1").on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        $('#thunamtp2').timepicker('setTime', (hour + 1) + ':' + e.time.minutes);
        if (hour < 7) {
            $('#thunamtp1').timepicker('setTime', '7:' + e.time.minutes + ' AM');
        }
        else if (hour > 18) {
            $('#thunamtp1').timepicker('setTime', '18:' + '00' + ' PM');
        }
    });

    $('#thunamtp2').on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        if (hour < 8) {
            $('#thunamtp2').timepicker('setTime', '8:' + e.time.minutes + ' AM');
        }
        else if (hour > 21) {
            $('#thunamtp2').timepicker('setTime', '21:' + '00' + ' PM');
        }
    });
    //Thu sau
    $("#thusautp1").on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        $('#thusautp2').timepicker('setTime', (hour + 1) + ':' + e.time.minutes);
        if (hour < 7) {
            $('#thusautp1').timepicker('setTime', '7:' + e.time.minutes + ' AM');
        }
        else if (hour > 18) {
            $('#thusautp1').timepicker('setTime', '18:' + '00' + ' PM');
        }
    });

    $('#thusautp2').on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        if (hour < 8) {
            $('#thusautp2').timepicker('setTime', '8:' + e.time.minutes + ' AM');
        }
        else if (hour > 21) {
            $('#thusautp2').timepicker('setTime', '21:' + '00' + ' PM');
        }
    });
    //Thu bay
    $("#thubaytp1").on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        $('#thubaytp2').timepicker('setTime', (hour + 1) + ':' + e.time.minutes);
        if (hour < 7) {
            $('#thubaytp1').timepicker('setTime', '7:' + e.time.minutes + ' AM');
        }
        else if (hour > 18) {
            $('#thubaytp1').timepicker('setTime', '18:' + '00' + ' PM');
        }
    });

    $('#thubaytp2').on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        if (hour < 8) {
            $('#thubaytp2').timepicker('setTime', '8:' + e.time.minutes + ' AM');
        }
        else if (hour > 21) {
            $('#thubaytp2').timepicker('setTime', '21:' + '00' + ' PM');
        }
    });
    //Chu nhat
    $("#chunhattp1").on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        $('#chunhattp2').timepicker('setTime', (hour + 1) + ':' + e.time.minutes);
        if (hour < 7) {
            $('#chunhattp1').timepicker('setTime', '7:' + e.time.minutes + ' AM');
        }
        else if (hour > 18) {
            $('#chunhattp1').timepicker('setTime', '18:' + '00' + ' PM');
        }
    });

    $('#chunhattp2').on('changeTime.timepicker', function (e) {
        var hour = e.time.hours;
        if (e.time.meridian === "PM" && hour !== 12) {
            hour += 12;
        }
        hour += (e.time.minutes / 100);
        if (hour < 8) {
            $('#chunhattp2').timepicker('setTime', '8:' + e.time.minutes + ' AM');
        }
        else if (hour > 21) {
            $('#chunhattp2').timepicker('setTime', '21:' + '00' + ' PM');
        }
    });
//});