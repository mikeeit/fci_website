﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class StatisticVM
    {
        public int CourseNo { get; set; }
        public Course Course { get; set; }
        public IEnumerable<Course> Courses { get; set; }
        public IEnumerable<DateTime> CourseDate { get; set; }
        public List<List<bool>> lstchk { get; set; }
        public int[] rowcntNAbs { get; set; }
        public int[] rowcntAbs { get; set; }
    }
}