﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class Events
    {
        public int idSchedule { get; set; }
        public string CourseName { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int DOW { get; set; }
    }
}