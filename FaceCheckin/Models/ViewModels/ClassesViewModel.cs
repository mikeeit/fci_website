﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class ClassesViewModel
    {
    }
    public partial class FacultyView
    {
        public string idFaculty { get; set; }
        public string FacultyName { get; set; }
    }
    public partial class DepartmentViewModels
    {
        public string idDepartment { get; set; }
        public string DepartmentName { get; set; }
        public string idFaculty { get; set; }
    }
    public class StdSumVM
    {
        public Student Student { get; set; }
        public List<OneCrs> Courses { get; set; }
    }
    public class OneCrs
    {
        public string idCourse { get; set; }
        public string CourseName { get; set; }
        public string AbsSum { get; set; }
        public string  LateSum { get; set; }
        public string SchoolYear { get; set; }
    }
    public class ClassesVM
    {
        public Class Class { get; set; }
        public IEnumerable<Instructor> Instructors { get; set; }
        public IEnumerable<Department> Departments { get; set; }
    }
}