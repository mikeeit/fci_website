﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class StudentViewModel
    {
        public Student Student { get; set; }
        public bool chuyenlop { get; set; }
        public IEnumerable<Class> Classes { get; set; }

    }
}