﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class FacultyViewModel
    {
        [Required]
        public string idFaculty { get; set; }
        [Required]
        public string FacultyName { get; set; }
        public string I_Name { get; set; }
    }
}