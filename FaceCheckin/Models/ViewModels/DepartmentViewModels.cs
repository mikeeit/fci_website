﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class DepartmentViewModel
    {
    }
    public class FacultyViewModels
    {
        public string idFaculty { get; set; }
         [Required]
        public string FacultyName { get; set; }
    }
    public class DeptVM
    {
        public Department Department { get; set; }
        public IEnumerable<Faculty> Faculties { get; set; }
    }
}