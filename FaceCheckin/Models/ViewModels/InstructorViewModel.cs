﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class InstructorViewModel
    {
        public Instructor Instructor { get; set; }
        public IEnumerable<Department> Departments { get; set; }
    }

}