﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FaceCheckin.Models.ViewModels
{
    public class CourseViewModel
    {
        public Course Course { get; set; }
        public IEnumerable<Instructor> Instructors { get; set; }
        public IEnumerable<Department> Departments { get; set; }
        public IEnumerable<Schedule> Schedules { get; set; }
    }
    public class CourseDetailsVM
    {
        public int CourseNo { get; set; }
        public Course Course { get; set; }
        public IEnumerable<DateTime> CourseDate { get; set; }
        public List<List<bool>> lstchk { get; set; }
        public List<List<string>> lstlate { get; set; }
        public int[] rowcntNAbs { get; set; }
        public int[] rowcntAbs { get; set; }
        public int[] clmncntNAbs { get; set; }
    }
    public class AddStdExcel
    {
        public string idStudent { get; set; }
        public string S_Name { get; set; }
        public string idCourse { get; set; }
        public string SchoolYear { get; set; }
        public int idSC { get; set; }
    }
    public class StdSum
    {
        public string idStudent { get; set; }
        public Course Course { get; set; }
        public string currentCourseNo { get; set; }
        public string S_Name { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> S_Dob { get; set; }
        public int? NoAbsent { get; set; }
        public int? NoLate { get; set; }
        public int? NoCourse { get; set; }
        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public IEnumerable<DateTime> CourseDate { get; set; }
        public List<string> tick { get; set; }
        public int? absWarning { get; set; }
        public int? lateWarning { get; set; }
    }
    public class DetailStdVM
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CrsIndex { get; set; }
        public bool isAvailable { get; set; }
    }
    public class DocumentModelView
    {
        public HttpPostedFileBase ExcelFileName { get; set; }
    }
    public class AddStdVM
    {
        public bool Selected { get; set; }
        public string idStudent { get; set; }
        public string S_Name { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> S_Dob { get; set; }
        public string ClassName { get; set; }
        public string idCourse { get; set; }
        public string SchoolYear { get; set; }
        public int NoLate { get; set; }
        public int NoAbsent { get; set; }
        public int idSC { get; set; }
    }
    public class StdAddListVM
    {
        public List<AddStdVM> StdList { get; set; }
    }
    public class AddCourseVM
    {
        public string Mon1 { get; set; }
        public string Tue1 { get; set; }
        public string Wed1 { get; set; }
        public string Thu1 { get; set; }
        public string Fri1 { get; set; }
        public string Sat1 { get; set; }
        public string Sun1 { get; set; }
        public string Mon2 { get; set; }
        public string Tue2 { get; set; }
        public string Wed2 { get; set; }
        public string Thu2 { get; set; }
        public string Fri2 { get; set; }
        public string Sat2 { get; set; }
        public string Sun2 { get; set; }
        public bool chk1 { get; set; }
        public bool chk2 { get; set; }
        public bool chk3 { get; set; }
        public bool chk4 { get; set; }
        public bool chk5 { get; set; }
        public bool chk6 { get; set; }
        public bool chk7 { get; set; }
        public string idDepartment { get; set; }
        public string DepartmentName { get; set; }
        public Course Course { get; set; }
        public IEnumerable<Instructor> Instructors { get; set; }
        public IEnumerable<Department> Departments { get; set; }
        public IEnumerable<Schedule> Schedules { get; set; }
    }
}