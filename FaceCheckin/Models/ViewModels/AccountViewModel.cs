﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security; 
namespace FaceCheckin.Models.ViewModels
{
    public class AccountViewModel
    {
        public class LoginViewModel
        {
            [Display(Name = "Tên đăng nhập")]
            [Required(ErrorMessage = "Không được để trống!")]
            [RegularExpression(@"^\S*$", ErrorMessage = "Không được chứa ký tự trống")]
            public string UserName { get; set; }
            [Display(Name = "Tên đăng nhập")]
            [Required(ErrorMessage = "Không được để trống!")]
            [RegularExpression(@"^\S*$", ErrorMessage = "Không được chứa ký tự trống")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Nhớ đăng nhập")]
            public bool RememberMe { get; set; }
        }
       public class ChangePasswordViewModel
        {
            public Account Accounts { get; set; }
            [Required(ErrorMessage = "Không được để trống!")]
            [StringLength(100, ErrorMessage = "Mật khẩu phải có ít nhất 6 ký tự!", MinimumLength = 6)]
            [RegularExpression(@"^\S*$", ErrorMessage = "Không được chứa ký tự trống")]
            [DataType(DataType.Password)]
            [Display(Name = "Mật khẩu cũ")]
            public string OldPassword { get; set; }
            [Required(ErrorMessage = "Không được để trống!")]
            [StringLength(100, ErrorMessage = "Mật khẩu phải có ít nhất 6 ký tự!", MinimumLength = 6)]
            [RegularExpression(@"^\S*$", ErrorMessage = "Không được chứa ký tự trống")]
            [DataType(DataType.Password)]
            [Display(Name = "Mật khẩu mới")]
            public string NewPassword { get; set; }
            [Required(ErrorMessage = "Không được để trống!")]
            [DataType(DataType.Password)]
            [Display(Name = "Xác nhận mật khẩu mới")]
            [StringLength(100, ErrorMessage = "Mật khẩu phải có ít nhất 6 ký tự!", MinimumLength = 6)]
            [RegularExpression(@"^\S*$", ErrorMessage = "Không được chứa ký tự trống")]
            [Compare("NewPassword", ErrorMessage = "Xác nhận lại mật khẩu!")]
            public string ConfirmPassword { get; set; }
        }
    }
    public class AccountVM
    {
        public Account Account { get; set; }
        public IEnumerable<Role> Roles { get; set; }

    }
    public class DetailsInformationVM
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string I_Name { get; set; }
        public string I_Phone { get; set; }
        public string I_Email { get; set; }
        public string Password { get; set; }
        public string DepartmentName { get; set; }
        public string FacultyName { get; set; }
    }
}