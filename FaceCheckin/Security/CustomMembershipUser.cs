﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace FaceCheckin.Security
{
    public class CustomMembershipUser : MembershipUser
    {
        #region Properties

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int UserRoleId { get; set; }

        public string UserRoleName { get; set; }

        #endregion

        public CustomMembershipUser(Account user)
            : base("CustomMembershipProvider", user.UserName, user.idAccount, string.Empty, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            //FirstName = user.Instructor.I_Name;
            UserRoleId = user.Role.idRole;
            UserRoleName = user.Role.RoleName;
        }
    }
}