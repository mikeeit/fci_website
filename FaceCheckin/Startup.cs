﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FaceCheckin.Startup))]
namespace FaceCheckin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
