﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq.Dynamic;
using FaceCheckin.Models.ViewModels;
using FaceCheckin.Security;

namespace FaceCheckin.Controllers
{
    [CustomAuthorize(Roles = "Admin,Trưởng khoa,Phó khoa")]
    public class DepartmentController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            var viewModel = CustomFacu();
            return View(viewModel);
        }
        private DeptVM CustomFacu()
        {
            var facu = db.Faculties.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                facu = (from f in db.Faculties
                        join d in db.Departments on f.idFaculty equals d.idFaculty
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select f).ToList();
            }
            var viewModel = new DeptVM
            {
                Faculties = facu
            };
            return viewModel;
        }
        [HttpPost]
        public ActionResult getDepartments()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idDepartment = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var DepartmentName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var FacultyName = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            var dept = (from e in db.Departments
                        join f in db.Faculties on e.idFaculty equals f.idFaculty
                        select new
                        {
                            idDepartment = e.idDepartment,
                            DepartmentName = e.DepartmentName,
                            FacultyName = f.FacultyName
                        }).ToList();
            //SEARCHING...
            if (!string.IsNullOrEmpty(idDepartment))
            {
                dept = dept.Where(a => a.idDepartment.Contains(idDepartment)).ToList();
            }
            if (!string.IsNullOrEmpty(DepartmentName))
            {
                dept = dept.Where(a => a.DepartmentName.Contains(DepartmentName)).ToList();
            }
            if (!string.IsNullOrEmpty(FacultyName))
            {
                dept = dept.Where(a => a.FacultyName.ToString()==FacultyName.ToString()).ToList();
            }

            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                dept = dept.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }

            TotalRecords = dept.ToList().Count();
            var NewItems = dept.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize(Roles = "Admin,Trưởng khoa")]
        public ActionResult Create(string parameterID)
        {
            var facu = db.Faculties.Single(c => c.idFaculty == parameterID).FacultyName;
            var viewModel = new Department
            {
                idFaculty = parameterID,
                FacultyName = facu
            };
            return PartialView("_CreateDepartment",viewModel);
        }
        [CustomAuthorize(Roles = "Admin,Trưởng khoa")]
        [HttpPost]
        public ActionResult Create(Department dept)
        {
            try
            {
                var facu = db.Faculties.ToList();
                dept.Faculties = facu;
                bool departmentExists = db.Departments.Any(fa => fa.idDepartment.Equals(dept.idDepartment));

                if (departmentExists)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = "Đã tồn tại Bộ môn", Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        db.Departments.Add(dept);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateDepartment", dept);
                }
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = ex.Message, Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };

            }
        }

        public ActionResult Edit(string id)
        {
            try
            {
                if (id == "")
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Department dept = db.Departments.Find(id);
                if (dept == null)
                    return HttpNotFound();
                var facu = db.Faculties.ToList();
                if (!User.IsInRole("Admin"))
                {
                    string tmp = Session["idIns"].ToString();
                    facu = (from f in db.Faculties
                            join d in db.Departments on f.idFaculty equals d.idFaculty
                            join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                            where i.idInstructor == tmp
                            select f).ToList();
                }
                DeptVM viewModel = new DeptVM()
                {
                    Faculties = facu,
                    Department = dept
                };
                return PartialView("_EditDepartment", viewModel);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult getDeptDetails(string id)
        {
            try
            {
                var rslt = (from e in db.Departments
                            join f in db.Faculties on e.idFaculty equals f.idFaculty
                            where e.idDepartment == id.ToString()
                            select new
                            {
                                DepartmentName = e.DepartmentName,
                                FacultyName = f.FacultyName
                            });
                return Json(rslt, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult deleteConfirmed(string id)
        {
            try
            {
                var rslt = db.Departments.FirstOrDefault(x => x.idDepartment == id.ToString());
                if (rslt != null)
                {
                    db.Departments.Remove(rslt);
                    db.SaveChanges();
                    return Json("Xóa thành công!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        [HttpPost]
        public ActionResult Edit(DeptVM dept)
        {
            try
            {
                dept.Faculties = db.Faculties.ToList();
                if (ModelState.IsValid)
                {
                    db.Entry(dept.Department).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(new { success = true, message = "Cập nhật thành công!" });
                }
                return PartialView("_EditDepartment", dept);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult getDetails(string id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idClass = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var ClassName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var SchoolYear = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;

            var crs = (from c in db.Departments
                       join st in db.Classes on c.idDepartment equals st.idDepartment
                       join ins in db.Instructors on st.idInstructor equals ins.idInstructor
                       where st.idDepartment == id
                       select new
                       {
                           idClass=st.idClass,
                           ClassName=st.ClassName,
                           I_Name=ins.I_Name,
                           SchoolYear=st.SchoolYear,
                           idInstructor=st.idInstructor
                       }).ToList();
            if (!string.IsNullOrEmpty(idClass))
            {
                crs = crs.Where(a => a.idClass.Contains(idClass)).ToList();
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                crs = crs.Where(a => a.ClassName.Contains(ClassName)).ToList();
            }
            if (!string.IsNullOrEmpty(SchoolYear))
            {
                crs = crs.Where(a => a.SchoolYear == SchoolYear).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                crs = crs.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }
            TotalRecords = crs.ToList().Count();
            var NewItems = crs.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(string id)
        {
            var schy = db.Classes.ToList().GroupBy(test => test.SchoolYear).Select(grp => grp.First());
            ViewBag.SchList = schy;
            var dept = db.Departments.Single(c => c.idDepartment == id);
            return View(dept);
        }
        private void getFacu()
        {
            ViewBag.facu = db.Faculties.ToList();
        }
    }
}