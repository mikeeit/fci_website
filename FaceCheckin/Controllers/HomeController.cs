﻿using FaceCheckin.Models.DB;
using FaceCheckin.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace FaceCheckin.Controllers
{
    public class HomeController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult getCalendar()
        {
            var lstdept = (from c in db.Courses
                           join s in db.Schedules on c.idCourse equals s.idCourse
                           select new Events
                           {
                               CourseName = c.CourseName,
                               idSchedule = s.idSchedule,
                               StartDate = c.StartDate.ToString(),
                               EndDate = c.EndDate.ToString(),
                               DOW = 0,
                               StartHour = "",
                               EndHour = "",
                           }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Thanh tra"))
            {
                string tmp = Session["idIns"].ToString();
                lstdept = (from c in db.Courses
                           join s in db.Schedules on c.idCourse equals s.idCourse
                           where c.idInstructor == tmp
                           select new Events
                           {
                               CourseName = c.CourseName,
                               idSchedule = s.idSchedule,
                               StartDate = c.StartDate.ToString(),
                               EndDate = c.EndDate.ToString(),
                               DOW = 0,
                               StartHour = "",
                               EndHour = "",
                           }).ToList();
            }

            List<object> listD = new List<object>();

            foreach (var dept in lstdept)
            {
                var sch = db.Schedules.Where(s => s.idSchedule == dept.idSchedule).FirstOrDefault();
                Events res = null;
                res = createE(sch.Mon, 1, dept);
                if (res != null) listD.Add(res);
                res = createE(sch.Tue, 2, dept);
                if (res != null) listD.Add(res);
                res = createE(sch.Wed, 3, dept);
                if (res != null) listD.Add(res);
                res = createE(sch.Thu, 4, dept);
                if (res != null) listD.Add(res);
                res = createE(sch.Fri, 5, dept);
                if (res != null) listD.Add(res);
                res = createE(sch.Sat, 6, dept);
                if (res != null) listD.Add(res);
                res = createE(sch.Sun, 0, dept);
                if (res != null) listD.Add(res);
            }
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(listD);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private Events createE(string thu, int ma, Events dept)
        {
            if (thu != null)
            {
                var one = thu.Split(';');
                one = one[0].Split('-');
                return (new Events
                {
                    CourseName = dept.CourseName,
                    idSchedule = dept.idSchedule,
                    StartDate = dept.StartDate,
                    EndDate = dept.EndDate,
                    DOW = ma,
                    StartHour = one[0],
                    EndHour = one[1],
                });
            }
            return null;
        }

        [HttpPost]
        public ActionResult ChangePassword(FaceCheckin.Models.ViewModels.AccountViewModel.ChangePasswordViewModel cp)
        {
            try
            {
                var idacc = Session["idAccount"];
                if (ModelState.IsValid)
                {
                    var dbUser = db.Accounts.Find(idacc);
                    if (dbUser.Password != cp.OldPassword)
                    {
                        TempData["error"] = "Mật khẩu cũ không đúng!";
                    }
                    else
                    {
                        dbUser.Password = cp.NewPassword;
                        db.Entry(dbUser).State = EntityState.Modified;
                        db.SaveChanges();
                        TempData["success"] = "Đổi mật khẩu thành công!";
                    }
                }
                else
                    TempData["error"] = "Đổi mật khẩu thất bại!";
                return View();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult DetailsInformation()
        {
            string idss = Session["idAccount"].ToString();
            var ac = (from i in db.Accounts
                      join ab in db.Roles on i.idRole equals ab.idRole
                      join ins in db.Instructors on i.idAccount equals ins.I_idAccount
                      join de in db.Departments on ins.I_idDepartment equals de.idDepartment
                      join fa in db.Faculties on de.idFaculty equals fa.idFaculty
                      where i.idAccount != 1 && i.idAccount.ToString() == idss
                      select new DetailsInformationVM
                      {
                          UserName = i.UserName,
                          RoleName = ab.RoleName,
                          Password = i.Password,
                          I_Name = ins.I_Name,
                          I_Email = ins.I_Email,
                          I_Phone = ins.I_Phone,
                          DepartmentName = de.DepartmentName,
                          FacultyName = fa.FacultyName
                      }).First();
            return View(ac);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return SignOut();
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(FaceCheckin.Models.ViewModels.AccountViewModel.LoginViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.RedirectFromLoginPage(model.UserName, model.RememberMe);
                    var dataItem = db.Accounts.Where(x => x.UserName.ToLower().Equals(model.UserName.ToLower()) && x.Password.Equals(model.Password) && x.Status == true).FirstOrDefault();
                    string tmp = "", nm = "";
                    Session["idAccount"] = dataItem.idAccount;
                    Session["RoleName"] = dataItem.Role.RoleName;
                    if (dataItem.Role.RoleName == "Admin")
                    {
                        tmp = "System Admin";
                        //nm = "";
                    }
                    else
                    {
                        tmp = db.Instructors.Where(x => x.I_idAccount == dataItem.idAccount).SingleOrDefault().I_Name.ToString();
                        nm = db.Instructors.Where(x => x.I_idAccount == dataItem.idAccount).SingleOrDefault().idInstructor.ToString();
                    }
                    Session["idIns"] = nm;
                    Session["Name"] = tmp;
                    Session["idRole"] = dataItem.Role.idRole;
                }

                ViewBag.Error = "Đăng nhập thất bại, vui lòng kiểm tra tài khoản/ mật khẩu!";
            }

            return View(model);
        }
        //[HttpPost]
        //public ActionResult Login(FaceCheckin.Models.ViewModels.AccountViewModel.LoginViewModel model, string returnUrl)
        //{
        //    var dataItem = db.Accounts.Where(x => x.UserName.ToLower().Equals(model.UserName.ToLower()) && x.Password.Equals(model.Password) && x.Status == true).FirstOrDefault();
        //    if (dataItem != null)
        //    {
        //        FormsAuthentication.SetAuthCookie(dataItem.UserName, false);
        //        string tmp = "", nm = "";
        //        Session["idAccount"] = dataItem.idAccount;
        //        Session["RoleName"] = dataItem.Role.RoleName;
        //        if (dataItem.Role.RoleName == "Admin")
        //        {
        //            tmp = "System Admin";
        //            nm = "";
        //        }
        //        else
        //        {
        //            tmp = db.Instructors.Where(x => x.I_idAccount == dataItem.idAccount).SingleOrDefault().I_Name.ToString();
        //            nm = db.Instructors.Where(x => x.I_idAccount == dataItem.idAccount).SingleOrDefault().idInstructor.ToString();
        //        }
        //        Session["idIns"] = nm;
        //        Session["Name"] = tmp;
        //        Session["idRole"] = dataItem.Role.idRole;
        //        return RedirectToAction("Index");
        //        //  }

        //    }
        //    else
        //    {
        //        ViewBag.Error = "Đăng nhập thất bại, vui lòng kiểm tra tài khoản/ mật khẩu!";
        //        return View();
        //    }
        //}
        //[Authorize]
        //[HttpPost]
        [AllowAnonymous]
        public ActionResult SignOut()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Home", null);
        }

    }
}