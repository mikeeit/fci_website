﻿using FaceCheckin.Models.DB;
using FaceCheckin.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FaceCheckin.Controllers
{
    public class StatisticController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();
        //
        // GET: /Statistic/
        public ActionResult Index()
        {
            var facu = CustomFacu();
            ViewBag.FacuList = facu;
            var schy = db.Courses.ToList().GroupBy(test => test.SchoolYear).Select(grp => grp.First());
            ViewBag.SchList = schy;
            return View();
        }
        private List<Faculty> CustomFacu()
        {
            var facu = db.Faculties.ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Thanh tra"))
            {
                string tmp = Session["idIns"].ToString();
                facu = (from f in db.Faculties
                        join d in db.Departments on f.idFaculty equals d.idFaculty
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select f).ToList();
            }
            return facu;
        }
        [HttpPost]
        public ActionResult getDeptList(string id)
        {
            var lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa") && !User.IsInRole("Thanh tra"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString() && e.idDepartment == idf.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstdept);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private string getIdDept(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       where i.idInstructor == tmp
                       select d.idDepartment).FirstOrDefault();
            return idf;
        }
        [HttpPost]
        public ActionResult getCourseList(string idDepartment, string SchoolYear)
        {
            var lstclass = (from e in db.Courses
                            where e.idDepartment == idDepartment.ToString() && e.SchoolYear == SchoolYear.ToString()
                            select new
                            {
                                e.idCourse,
                                e.CourseName
                            }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa") && !User.IsInRole("Thanh tra"))
            {
                lstclass = (from e in db.Courses
                            where e.idDepartment == idDepartment.ToString() && e.SchoolYear == SchoolYear.ToString()
                            select new
                            {
                                e.idCourse,
                                e.CourseName
                            }).ToList();
                if (User.IsInRole("Giảng viên") || User.IsInRole("Trợ giảng"))
                {
                    string tmp = Session["idIns"].ToString();
                    var idf = getIdDept(tmp);
                    lstclass = (from e in db.Courses
                                where e.idDepartment == idDepartment.ToString() && e.SchoolYear == SchoolYear.ToString() && e.idInstructor == tmp
                                select new
                                {
                                    e.idCourse,
                                    e.CourseName
                                }).ToList();
                }
            }
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstclass);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private IEnumerable<DateTime> ListDateOfCourse(DateTime start, DateTime end, string idCourse)
        {
            string[] dow = new string[7];
            var lstdow = (from c in db.Courses
                          join sch in db.Schedules on c.idCourse equals sch.idCourse
                          where c.idCourse == idCourse
                          select sch).FirstOrDefault();
            dow[0] = lstdow.Mon;
            dow[1] = lstdow.Tue;
            dow[2] = lstdow.Wed;
            dow[3] = lstdow.Thu;
            dow[4] = lstdow.Fri;
            dow[5] = lstdow.Sat;
            dow[6] = lstdow.Sun;
            List<DayOfWeek> daysToChoose = new List<DayOfWeek>();
            if (dow[0] != null)
                daysToChoose.Add(DayOfWeek.Monday);
            if (dow[1] != null)
                daysToChoose.Add(DayOfWeek.Tuesday);
            if (dow[2] != null)
                daysToChoose.Add(DayOfWeek.Wednesday);
            if (dow[3] != null)
                daysToChoose.Add(DayOfWeek.Thursday);
            if (dow[4] != null)
                daysToChoose.Add(DayOfWeek.Friday);
            if (dow[5] != null)
                daysToChoose.Add(DayOfWeek.Saturday);
            if (dow[6] != null)
                daysToChoose.Add(DayOfWeek.Sunday);
            var dates = Enumerable.Range(0, (int)(end - start).TotalDays + 1)
                            .Select(d => start.AddDays(d))
                            .Where(d => daysToChoose.Contains(d.DayOfWeek));
            return dates;
        }
        public ActionResult PartialSta(string parameterID)
        {
            if (parameterID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var crs = db.Courses.Single(c => c.idCourse == parameterID);
            IEnumerable<DateTime> dow = ListDateOfCourse(crs.StartDate, crs.EndDate, parameterID);
            int lth = dow.Count();
            var now = DateTime.Now;
            int[] cnt = new int[lth];
            int[] cntabs = new int[lth];
            foreach (var i in crs.STUDENT_COURSE)
            {
                int cr = 0;
                //i.chksch = new List<bool?>(lth);
                i.chksch = new List<string>(lth);
                foreach (var dt in dow)
                {
                    var tmp = db.Trackings.Where(c => c.idSC.Equals(i.idSC)).ToList();
                    if (DateTime.Compare(now.Date, dt.Date) >= 0)
                    {
                        var tt = tmp.Where(x => x.Logs.ToShortDateString() == dt.ToShortDateString()).FirstOrDefault();
                        if (tt != null)
                        {
                            if (tt.isLate != "0")
                            {
                                i.chksch.Add(tt.isLate);
                            }
                            else
                            {
                                i.chksch.Add("true");
                            }
                            cnt[cr] += 1;
                        }
                        else
                        {
                            i.chksch.Add("false");
                            cntabs[cr] += 1;
                        }
                    }
                    else
                        i.chksch.Add(null);
                    cr++;
                }
            }
            List<string> cc = new List<string>();
            foreach (var t in dow)
            {
                cc.Add("'"+t.ToString("dd/MM")+"'");
            }
            string yaxis = string.Join(",", cntabs);
            string xaxis = string.Join(",", cc);
            ViewBag.CountABS = yaxis.Trim();
            ViewBag.CourseDate = xaxis.Trim();  
            var viewModel = new StatisticVM
            {
                Course = crs,
                CourseDate = dow,
                rowcntNAbs = cnt,
                rowcntAbs = cntabs,
                CourseNo = lth
            };
            if (crs == null)
            {
                return HttpNotFound();
            }
            return PartialView("statisticAndChart", viewModel);
        }
    }
}