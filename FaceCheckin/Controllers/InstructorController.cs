﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq.Dynamic;
using FaceCheckin.Models.ViewModels;
using FaceCheckin.Security;
using System.Web.Script.Serialization;
using System.Text;

namespace FaceCheckin.Controllers
{
    [CustomAuthorize(Roles = "Admin,Trưởng khoa,Phó khoa,Trưởng bộ môn,Phó bộ môn")]
    public class InstructorController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            var facu = CustomFacu();
            ViewBag.FacuList = facu;
            return View();
        }

        private List<Faculty> CustomFacu()
        {
            var facu = db.Faculties.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                facu = (from f in db.Faculties
                        join d in db.Departments on f.idFaculty equals d.idFaculty
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select f).ToList();
            }
            return facu;
        }

        [HttpPost]
        public ActionResult getDeptList(string id)
        {
            var lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString() && e.idDepartment == idf.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstdept);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getInsList(string id)
        {
            var lstins = (from e in db.Instructors
                          where e.I_idDepartment == id.ToString()
                          select new
                          {
                              e.idInstructor,
                              e.I_Name
                          }).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstins);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getInstructors()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idInstructor = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var I_Name = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var I_Email = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var I_Phone = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var DepartmentName = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            var insts = (from i in db.Instructors
                         join ab in db.Departments on i.I_idDepartment equals ab.idDepartment
                         select new
                         {
                             idInstructor = i.idInstructor,
                             I_Name = i.I_Name,
                             I_Email = i.I_Email,
                             I_Phone = i.I_Phone,
                             DepartmentName = ab.DepartmentName
                         }).ToList();
            //SEARCHING...
            if (!string.IsNullOrEmpty(idInstructor))
            {
                insts = insts.Where(a => a.idInstructor.Contains(idInstructor)).ToList();
            }
            if (!string.IsNullOrEmpty(I_Name))
            {
                insts = insts.Where(a => a.I_Name.Contains(I_Name)).ToList();
            }
            if (!string.IsNullOrEmpty(DepartmentName))
            {
                insts = insts.Where(a => a.DepartmentName.ToString() == DepartmentName.ToString()).ToList();
            }
            if (!string.IsNullOrEmpty(I_Email))
            {
                insts = insts.Where(a => a.I_Email.Contains(I_Email)).ToList();
            }
            if (!string.IsNullOrEmpty(I_Phone))
            {
                insts = insts.Where(a => a.I_Phone.Contains(I_Phone)).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                insts = insts.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }

            TotalRecords = insts.ToList().Count();
            var NewItems = insts.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        private InstructorViewModel CustomDept()
        {
            var dept = db.Departments.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                dept = (from d in db.Departments
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select d).ToList();
                if (!User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
                {
                    dept = (from e in db.Departments
                            where e.idDepartment == idf.ToString()
                            select e).ToList();
                }
            }
            var viewModel = new InstructorViewModel
            {
                Departments = dept
            };
            return viewModel;
        }
        public ActionResult Create(string parameterID)
        {
            var facu = db.Departments.Single(c => c.idDepartment == parameterID).DepartmentName;
            var viewModel = new Instructor
            {
                I_idDepartment = parameterID,
                DepartmentName = facu
            };
            return PartialView("_CreateInstructor", viewModel);
        }

        [HttpPost]
        public ActionResult Create(Instructor instr)
        {
            try
            {
                //var dept = db.Departments.ToList();
                //instr.Departments = dept;
                bool instrExists = db.Instructors.Any(fa => fa.idInstructor.Equals(instr.idInstructor));
                if (instrExists)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = "Đã tồn tại Giảng viên", Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        db.Instructors.Add(instr);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateInstructor", instr);
                }
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = ex.Message, Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };

            }
            //  }

        }


        public ActionResult getInsDetails(string id)
        {
            try
            {
                var rslt = (from i in db.Instructors
                            join ab in db.Departments on i.I_idDepartment equals ab.idDepartment
                            where i.idInstructor == id.ToString()
                            select new
                            {
                                idInstructor = i.idInstructor,
                                I_Name = i.I_Name,
                                I_Email = i.I_Email,
                                I_Phone = i.I_Phone,
                                DepartmentName = ab.DepartmentName
                            }).ToList();
                return Json(rslt, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult deleteConfirmed(string id)
        {
            try
            {
                var rslt = db.Instructors.FirstOrDefault(x => x.idInstructor == id.ToString());
                if (rslt != null)
                {
                    db.Instructors.Remove(rslt);
                    db.SaveChanges();
                    return Json("Xóa thành công!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult Edit(string id)
        {
            try
            {
                if (id == "")
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Instructor instr = db.Instructors.Find(id);
                if (instr == null)
                    return HttpNotFound();
                //var dept = db.Departments.ToList();
                //if (!User.IsInRole("Admin"))
                //{
                //    string tmp = Session["idIns"].ToString();
                //    var idf = getIdFaculty(tmp);
                //    var idd = getIdDept(tmp);
                //    dept = db.Departments.Where(a => a.idFaculty == idf).ToList();
                //    if (!User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
                //    {
                //        dept = (from e in db.Departments
                //                where e.idDepartment == idd.ToString()
                //                select e).ToList();
                //    }
                //}
                //ViewBag.DeptList = new SelectList(dept, "idDepartment", "DepartmentName", instr.I_idDepartment);
                var viewModel = new InstructorViewModel
                {
                    //Departments = dept,
                    Instructor = instr
                };
                return PartialView("_EditInstructor", viewModel);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string getIdFaculty(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       join f in db.Faculties on d.idFaculty equals f.idFaculty
                       where i.idInstructor == tmp
                       select f.idFaculty).FirstOrDefault();
            return idf;
        }
        private string getIdDept(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       where i.idInstructor == tmp
                       select d.idDepartment).FirstOrDefault();
            return idf;
        }


        [HttpPost]
        public ActionResult Edit(InstructorViewModel instr)
        {
            try
            {
                //instr.Departments = db.Departments.ToList();
                if (ModelState.IsValid)
                {
                    var dbUser = db.Instructors.Find(instr.Instructor.idInstructor);
                    dbUser.I_Name = instr.Instructor.I_Name;
                    dbUser.I_Email = instr.Instructor.I_Email;
                    dbUser.I_Phone = instr.Instructor.I_Phone;
                    db.Entry(dbUser).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(new { success = true, message = "Cập nhật thành công!" });
                }
                return PartialView("_EditInstructor", instr);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instructor instr = db.Instructors.Find(id);
            if (instr == null)
            {
                return HttpNotFound();
            }
            return View(instr);
        }
    }
}