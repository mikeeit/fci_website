﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq.Dynamic;
using FaceCheckin.Models.ViewModels;
using FaceCheckin.Security;
using System.Web.Script.Serialization;
using System.Text;

namespace FaceCheckin.Controllers
{
     [CustomAuthorize(Roles = "Admin")]
    public class AccountController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            return View();
        }
       
        [HttpPost]
        public ActionResult getAccounts()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idAccount = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var UserName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var I_Name = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var RoleName = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            var ac = (from i in db.Accounts
                      join ab in db.Roles on i.idRole equals ab.idRole
                      join ins in db.Instructors on i.idAccount equals ins.I_idAccount
                      where i.idAccount != 1
                      select new
                      {
                          UserName = i.UserName,
                          RoleName = ab.RoleName,
                          Status = i.Status,
                          Password = i.Password,
                          idAccount = i.idAccount,
                          I_Name = ins.I_Name
                      }).ToList();
            if (!string.IsNullOrEmpty(UserName))
            {
                ac = ac.Where(a => a.UserName.ToLower().Contains(UserName.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(RoleName))
            {
                ac = ac.Where(a => a.RoleName.ToLower().Contains(RoleName.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(I_Name))
            {
                ac = ac.Where(a => a.I_Name.ToLower().Contains(I_Name.ToLower())).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                ac = ac.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }
            TotalRecords = ac.ToList().Count();
            var NewItems = ac.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Account acc = db.Accounts.Find(id);
                if (acc == null)
                    return HttpNotFound();
                var rslt = db.Roles.ToList();
                ViewBag.RoleList = new SelectList(rslt, "idRole", "RoleName", acc.idRole);
                List<SelectListItem> status = new List<SelectListItem>();
                status.Add(new SelectListItem() { Value = "true", Text = "Hoạt động" });
                status.Add(new SelectListItem() { Value = "false", Text = "Khóa" });
                ViewBag.status = new SelectList(status, "Value", "Text", acc.Status);
                var viewModel = new AccountVM
                {
                    Account = acc,
                    Roles = rslt
                };
                return PartialView("_EditAccount", viewModel);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult Edit(AccountVM acc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(acc.Account).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { success = true, message = "Cập nhật thành công!" });
                }
                return PartialView("_EditAccount", acc);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
	}
}