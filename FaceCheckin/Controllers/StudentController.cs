﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq.Dynamic;
using FaceCheckin.Models.ViewModels;
using FaceCheckin.Security;
using System.Web.Script.Serialization;
using System.Text;
using System.Data.OleDb;
using System.Data;
using LinqToExcel;
using System.Data.Entity.Validation;

namespace FaceCheckin.Controllers
{
    [CustomAuthorize(Roles = "Admin,Trưởng khoa,Phó khoa,Trưởng bộ môn,Phó bộ môn,Giảng viên,Trợ giảng")]
    public class StudentController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            var facu = CustomFacu();
            ViewBag.FacuList = facu;
            return View();
        }
        private List<Faculty> CustomFacu()
        {
            var facu = db.Faculties.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                facu = (from f in db.Faculties
                        join d in db.Departments on f.idFaculty equals d.idFaculty
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select f).ToList();
            }
            return facu;
        }

        [HttpPost]
        public ActionResult getDeptList(string id)
        {
            var lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString() && e.idDepartment == idf.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstdept);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getClassList(string id)
        {
            var lstclass = (from e in db.Classes
                            where e.idDepartment == id.ToString()
                            select new
                            {
                                e.idClass,
                                e.ClassName
                            }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
            {
                lstclass = (from e in db.Classes
                            where e.idDepartment == id.ToString()
                            select new
                            {
                                e.idClass,
                                e.ClassName
                            }).ToList();
                if (User.IsInRole("Giảng viên") || User.IsInRole("Trợ giảng"))
                {
                    string tmp = Session["idIns"].ToString();
                    var idf = getIdDept(tmp);
                    lstclass = (from e in db.Classes
                                where e.idDepartment == id.ToString() && e.idInstructor == tmp
                                select new
                                {
                                    e.idClass,
                                    e.ClassName
                                }).ToList();
                }

            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstclass);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getInsList(string id)
        {
            var lstins = (from e in db.Instructors
                          where e.I_idDepartment == id.ToString()
                          select new
                          {
                              e.idInstructor,
                              e.I_Name
                          }).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstins);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getStudents()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idStudent = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var S_Name = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var S_Email = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var S_Phone = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var ClassName = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            var stud = (from i in db.Students
                        join ab in db.Classes on i.idClass equals ab.idClass
                        select new
                        {
                            idStudent = i.idStudent,
                            S_Name = i.S_Name,
                            S_Email = i.S_Email,
                            S_Phone = i.S_Phone,
                            ClassName = ab.ClassName
                        }).ToList();
            //SEARCHING...
            if (!string.IsNullOrEmpty(idStudent))
            {
                stud = stud.Where(a => a.idStudent.Contains(idStudent)).ToList();
            }
            if (!string.IsNullOrEmpty(S_Name))
            {
                stud = stud.Where(a => a.S_Name.Contains(S_Name)).ToList();
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                stud = stud.Where(a => a.ClassName.ToString() == ClassName.ToString()).ToList();
            }
            if (!string.IsNullOrEmpty(S_Email))
            {
                stud = stud.Where(a => a.S_Email.Contains(S_Email)).ToList();
            }
            if (!string.IsNullOrEmpty(S_Phone))
            {
                stud = stud.Where(a => a.S_Phone.Contains(S_Phone)).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                stud = stud.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }

            TotalRecords = stud.ToList().Count();
            var NewItems = stud.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        private List<Department> CustomDept()
        {
            var dept = db.Departments.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                dept = (from d in db.Departments
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select d).ToList();
                if (!User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
                {
                    dept = (from e in db.Departments
                            where e.idDepartment == idf.ToString()
                            select e).ToList();
                }
            }
            return dept;
        }
        private StudentViewModel CustomClass()
        {
            var dept = db.Classes.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                dept = dept = (from e in db.Classes
                               join d in db.Departments on e.idDepartment equals d.idDepartment
                               where e.idDepartment == idf.ToString()
                               select e).ToList();
                if (!User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
                {
                    dept = (from e in db.Classes
                            join d in db.Departments on e.idDepartment equals d.idDepartment
                            where e.idDepartment == idf.ToString()
                            select e).ToList();
                }
            }
            var viewModel = new StudentViewModel
            {
                Classes = dept
            };
            return viewModel;
        }
        public ActionResult Create(string parameterID)
        {
            //var facu = CustomFacu();
            //ViewBag.FacuList = facu;
            //var dept = CustomDept();
            //ViewBag.DeptList = dept;
            var clsnm = db.Classes.Single(c => c.idClass == parameterID).ClassName;
            var viewModel = new Student
            {
                idClass = parameterID,
                ClassName=clsnm
            };
            return PartialView("_CreateStudent", viewModel);
        }

        [HttpPost]
        public ActionResult Create(Student instr)
        {
            try
            {
                var dept = db.Classes.ToList();
                instr.Classes = dept;
                bool stdExists = db.Students.Any(fa => fa.idStudent.Equals(instr.idStudent));
                if (stdExists)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = "Đã tồn tại Sinh viên", Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        db.Students.Add(instr);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateStudent", instr);
                }
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = ex.Message, Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };

            }
        }
        public ActionResult getStdDetails(string id)
        {
            try
            {
                var rslt = (from i in db.Students
                            join ab in db.Classes on i.idClass equals ab.idClass
                            where i.idStudent == id.ToString()
                            select new
                            {
                                idStudent = i.idStudent,
                                S_Name = i.S_Name,
                                S_Email = i.S_Email,
                                S_Phone = i.S_Phone,
                                ClassName = ab.ClassName
                            }).ToList();
                return Json(rslt, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult deleteConfirmed(string id)
        {
            try
            {
                var rslt = db.Students.FirstOrDefault(x => x.idStudent == id.ToString());
                if (rslt != null)
                {
                    db.Students.Remove(rslt);
                    db.SaveChanges();
                    return Json("Xóa thành công!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult Edit(string id)
        {
            try
            {
                if (id == "")
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                //string idr = Session["idRole"].ToString();
                Student std = db.Students.Find(id);
                if (std == null)
                    return HttpNotFound();
                // string tmp = Session["idIns"].ToString();
                //var idf = getIdFaculty(tmp);
                //var idd = getIdDept(tmp);
                var classes = db.Classes.ToList();
                //if (tmp != "")
                //{
                //    classes = db.Classes.Where(a => a.idDepartment == idd).ToList();
                //    if (idr != "1" && idr != "2" && idr != "3")
                //    {
                //        classes = (from e in db.Classes
                //                where e.idDepartment == idd.ToString()
                //                select e).ToList();
                //    }
                //}
                //var facu = CustomFacu();
                //ViewBag.FacuList = facu;
                //var dept = CustomDept();
                //ViewBag.DeptList = dept;
                ViewBag.ClassList = new SelectList(classes, "idClass", "ClassName", std.idClass);
                var viewModel = new StudentViewModel
                {
                    Classes = classes,
                    Student = std
                };
                return PartialView("_EditStudent", viewModel);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string getIdFaculty(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       join f in db.Faculties on d.idFaculty equals f.idFaculty
                       where i.idInstructor == tmp
                       select f.idFaculty).FirstOrDefault();
            return idf;
        }
        private string getIdDept(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       where i.idInstructor == tmp
                       select d.idDepartment).FirstOrDefault();
            return idf;
        }


        [HttpPost]
        public ActionResult Edit(StudentViewModel instr)
        {
            try
            {

                instr.Classes = db.Classes.ToList();
                // Response.Write("<script>alert('"+instr.Student.S_Dob+"')</script>");
                if (ModelState.IsValid)
                {
                    var dbUser = db.Students.Find(instr.Student.idStudent);
                    if (instr.chuyenlop == false)
                    {
                        dbUser.S_Name = instr.Student.S_Name;
                        dbUser.S_Dob = instr.Student.S_Dob;
                        dbUser.S_Email = instr.Student.S_Email;
                        dbUser.S_Phone = instr.Student.S_Phone;
                    }
                    else
                    {
                        dbUser.idClass = instr.Student.idClass;
                        dbUser.S_Name = instr.Student.S_Name;
                        dbUser.S_Dob = instr.Student.S_Dob;
                        dbUser.S_Email = instr.Student.S_Email;
                        dbUser.S_Phone = instr.Student.S_Phone;
                    }
                    db.Entry(dbUser).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { success = true, message = "Cập nhật thành công!" });
                }
                return PartialView("_EditStudent", instr);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student instr = db.Students.Find(id);
            if (instr == null)
            {
                return HttpNotFound();
            }
            return View(instr);
        }
        public FileResult DownloadExcel()
        {
            string path = "/Doc/MauExcelLopSinhHoat.xlsx";
            return File(path, "application/vnd.ms-excel", "MauExcelLopSinhHoat.xlsx");
        }
        public ActionResult AddListStudentToClassFromExcel()
        {
            return PartialView("_CreateListStudentFromExcel");
        }
        [HttpPost]
        public ActionResult AddListStudentToClassFromExcel(HttpPostedFileBase files)
        {
            List<string> data = new List<string>();
            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0)
                {
                    foreach (string file in Request.Files)
                    {
                        var _file = Request.Files[file];
                        if (_file != null)
                        {
                            string filename = _file.FileName;
                            string targetpath = Server.MapPath("~/Uploads/");
                            _file.SaveAs(targetpath + filename);
                            string pathToExcelFile = targetpath + filename;
                            var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);

                            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                            var ds = new DataSet();

                            adapter.Fill(ds, "ExcelTable");

                            DataTable dtable = ds.Tables["ExcelTable"];

                            string sheetName = "Sheet1";

                            var excelFile = new ExcelQueryFactory(pathToExcelFile);
                            excelFile.AddMapping<Student>(c => c.idStudent, "Mã sinh viên");
                            excelFile.AddMapping<Student>(c => c.S_Name, "Họ tên sinh viên");
                            excelFile.AddMapping<Student>(c => c.S_Dob, "Ngày sinh");
                            excelFile.AddMapping<Student>(c => c.S_Email, "Email");
                            excelFile.AddMapping<Student>(c => c.S_Phone, "Số điện thoại");
                            excelFile.AddMapping<Student>(c => c.idClass, "Lớp sinh hoạt");

                            var artistAlbums = from a in excelFile.Worksheet<Student>(sheetName) select a;

                            foreach (var a in artistAlbums)
                            {
                                try
                                {
                                    if ((a.idStudent != "" && a.S_Name != "" && a.idClass != "") || (a.idStudent != null && a.S_Name != null && a.idClass != null))
                                    {
                                        if (db.Students.Any(c => c.idStudent == a.idStudent && c.idClass == a.idClass))
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            Student TU = new Student();
                                            TU.idStudent = a.idStudent;
                                            TU.S_Name = a.S_Name;
                                            TU.S_Dob = a.S_Dob;
                                            TU.S_Email = a.S_Email == "" ? null : a.S_Email;
                                            TU.S_Phone = a.S_Phone == "" ? null : a.S_Phone;
                                            TU.idClass = a.idClass;
                                            db.Students.Add(TU);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        if (a.idStudent == "" || a.idStudent == null || a.S_Name == "" || a.S_Name == null || a.idClass == "" || a.idClass == null)
                                            continue;
                                    }
                                }

                                catch (DbEntityValidationException ex)
                                {
                                    return new JsonResult
                                    {
                                        Data = new { ErrorMessage = "Nhập từ file thất bại, vui lòng kiểm tra lại nội dung và định dạng file Excel", Success = false },
                                        ContentEncoding = System.Text.Encoding.UTF8,
                                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                                    };
                                }
                            }
                            //deleting excel file from folder  
                            if ((System.IO.File.Exists(pathToExcelFile)))
                            {
                                System.IO.File.Delete(pathToExcelFile);
                            }
                            return Json(new { success = true, message = "Nhập dữ liệu từ Excel thành công!" });
                        }
                        else
                        {
                            return new JsonResult
                            {
                                Data = new { ErrorMessage = "Vui lòng chọn file Excel", Success = false },
                                ContentEncoding = System.Text.Encoding.UTF8,
                                JsonRequestBehavior = JsonRequestBehavior.DenyGet
                            };
                        }
                    }

                }
            }
            return PartialView("_CreateListStudentFromExcel");
        }
    }
}