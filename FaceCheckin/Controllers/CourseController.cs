﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using System.Linq.Dynamic;
using FaceCheckin.Models.ViewModels;
using FaceCheckin.Security;
using System.Web.Script.Serialization;
using System.Data.OleDb;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using LinqToExcel;
using System.Data.Entity.Validation;

namespace FaceCheckin.Controllers
{
    [CustomAuthorize(Roles = "Admin,Trưởng khoa,Phó khoa,Trưởng bộ môn,Phó bộ môn,Giảng viên,Trợ giảng")]
    public class CourseController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();
        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            var facu = CustomFacu();
            ViewBag.FacuList = facu;
            var schy = db.Courses.ToList().GroupBy(test => test.SchoolYear).Select(grp => grp.First());
            ViewBag.SchList = schy;
            return View();
        }

        private List<Faculty> CustomFacu()
        {
            var facu = db.Faculties.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                facu = (from f in db.Faculties
                        join d in db.Departments on f.idFaculty equals d.idFaculty
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select f).ToList();
            }
            return facu;
        }

        [HttpPost]
        public ActionResult getDeptList(string id)
        {
            var lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString() && e.idDepartment == idf.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstdept);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getInsList(string id)
        {
            var lstins = (from e in db.Instructors
                          where e.I_idDepartment == id.ToString()
                          select new
                          {
                              e.idInstructor,
                              e.I_Name
                          }).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstins);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getCourses()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idCourse = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var CourseName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var InstructorName = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var DepartmentName = Request.Form.GetValues("columns[6][search][value]").FirstOrDefault();
            var SchoolYear = Request.Form.GetValues("columns[7][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            var course = (from e in db.Courses
                          join ab in db.Instructors on e.idInstructor equals ab.idInstructor into a
                          from ab in a.DefaultIfEmpty()
                          join f in db.Departments on e.idDepartment equals f.idDepartment
                          join i in db.Instructors on ab.idInstructor equals i.idInstructor into ic
                          from i in ic.DefaultIfEmpty()
                          select new
                          {
                              idCourse = e.idCourse,
                              CourseName = e.CourseName,
                              StartDate = e.StartDate,
                              EndDate = e.EndDate,
                              DepartmentName = f.DepartmentName,
                              SchoolYear = e.SchoolYear,
                              InstructorName = ab == null ? "(Chưa có GV)" : ab.I_Name
                          }).ToList();
            if (User.IsInRole("Giảng viên") || User.IsInRole("Trợ giảng"))
            {
                string tmp = Session["idIns"].ToString();
                course = (from e in db.Courses
                          join ab in db.Instructors on e.idInstructor equals ab.idInstructor into a
                          from ab in a.DefaultIfEmpty()
                          join f in db.Departments on e.idDepartment equals f.idDepartment
                          join i in db.Instructors on ab.idInstructor equals i.idInstructor into ic
                          from i in ic.DefaultIfEmpty()
                          where e.idInstructor == tmp
                          select new
                          {
                              idCourse = e.idCourse,
                              CourseName = e.CourseName,
                              StartDate = e.StartDate,
                              EndDate = e.EndDate,
                              DepartmentName = f.DepartmentName,
                              SchoolYear = e.SchoolYear,
                              InstructorName = ab == null ? "(Chưa có GV)" : ab.I_Name
                          }).ToList();
            }
            //SEARCHING...
            if (!string.IsNullOrEmpty(idCourse))
            {
                course = course.Where(a => a.idCourse.Contains(idCourse)).ToList();
            }
            if (!string.IsNullOrEmpty(CourseName))
            {
                course = course.Where(a => a.CourseName.Contains(CourseName)).ToList();
            }
            if (!string.IsNullOrEmpty(DepartmentName))
            {
                course = course.Where(a => a.DepartmentName.ToString() == DepartmentName.ToString()).ToList();
            }
            if (!string.IsNullOrEmpty(InstructorName))
            {
                course = course.Where(a => a.InstructorName.Contains(InstructorName)).ToList();
            }
            if (!string.IsNullOrEmpty(SchoolYear))
            {
                course = course.Where(a => a.SchoolYear == SchoolYear).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                course = course.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }


            TotalRecords = course.ToList().Count();
            var NewItems = course.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        private AddCourseVM CustomDept()
        {
            var dept = db.Departments.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                dept = (from d in db.Departments
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select d).ToList();
                if (!User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
                {
                    dept = (from e in db.Departments
                            where e.idDepartment == idf.ToString()
                            select e).ToList();
                }
            }
            var viewModel = new AddCourseVM
            {
                Departments = dept
            };
            return viewModel;
        }
        [CustomAuthorize(Roles = "Admin,Trưởng khoa,Trưởng bộ môn")]
        public ActionResult Create(string parameterID)
        {
            var facu = db.Departments.Single(c => c.idDepartment == parameterID).DepartmentName;
            var viewModel = new AddCourseVM
            {
                idDepartment = parameterID,
                DepartmentName = facu
            };
            return PartialView("_CreateCourse", viewModel);
        }
        [CustomAuthorize(Roles = "Admin,Trưởng khoa,Trưởng bộ môn")]
        [HttpPost]
        public ActionResult Create(AddCourseVM classes)
        {
            try
            {
                var dept = db.Departments.ToList();
                classes.Departments = dept;
                bool classExists = db.Courses.Any(fa => fa.idCourse.Equals(classes.Course.idCourse));
                //var start = new DateTime(2017, 4, 1);
                //var end = new DateTime(2017, 4, 30);

                if (classExists)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = "Đã tồn tại Lớp tín chỉ", Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        var sch = new Schedule();
                        sch.idCourse = classes.Course.idCourse;
                        sch.SchoolYear = classes.Course.SchoolYear;
                        sch.Mon = classes.Mon1 == null ? null : classes.Mon1 + "-" + classes.Mon2;
                        sch.Tue = classes.Tue1 == null ? null : classes.Tue1 + "-" + classes.Tue2;
                        sch.Wed = classes.Wed1 == null ? null : classes.Wed1 + "-" + classes.Wed2;
                        sch.Thu = classes.Thu1 == null ? null : classes.Thu1 + "-" + classes.Thu2;
                        sch.Fri = classes.Fri1 == null ? null : classes.Fri1 + "-" + classes.Fri2;
                        sch.Sat = classes.Sat1 == null ? null : classes.Sat1 + "-" + classes.Sat2;
                        sch.Sun = classes.Sun1 == null ? null : classes.Sun1 + "-" + classes.Sun2;

                        IEnumerable<DateTime> dow = ListDateOfCourse(classes.Course.StartDate, classes.Course.EndDate, sch);
                        int lth = dow.Count();


                        var cr = new Course();
                        cr.idCourse = classes.Course.idCourse;
                        cr.CourseName = classes.Course.CourseName;
                        cr.StartDate = classes.Course.StartDate;
                        cr.EndDate = classes.Course.EndDate;
                        cr.idInstructor = classes.Course.idInstructor;
                        cr.SchoolYear = classes.Course.SchoolYear;
                        cr.idDepartment = classes.idDepartment;
                        cr.NumberOfCourse = lth;

                        db.Courses.Add(cr);
                        db.SaveChanges();

                        db.Schedules.Add(sch);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateCourse", classes);
                }
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = ex.Message, Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };

            }
            //  }

        }


        public ActionResult getCourseDetails(string id)
        {
            try
            {
                var rslt = (from e in db.Courses
                            where e.idCourse == id.ToString()
                            select new
                            {
                                idCourse = e.idCourse,
                                CourseName = e.CourseName,
                            }).ToList();
                //var rslt = db.Faculties.Where(f => f.idFaculty.Equals(id));
                return Json(rslt, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult deleteConfirmed(string id)
        {
            try
            {
                var rslt = db.Courses.FirstOrDefault(x => x.idCourse == id.ToString());
                if (rslt != null)
                {
                    db.Courses.Remove(rslt);
                    db.SaveChanges();
                    return Json("Xóa thành công!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult Edit(string id)
        {
            try
            {
                if (id == "")
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var sch = db.Schedules.ToList();
                var sgsch = sch.Single(c => c.idCourse == id);
                var crs = db.Courses.Single(c => c.idCourse == id);
                if (crs == null)
                    return HttpNotFound();
                var ins = db.Instructors.Where(c => c.I_idDepartment == crs.idDepartment);
                string mon0 = null, mon1 = null, tue0 = null, tue1 = null, wed0 = null, wed1 = null, thu0 = null, thu1 = null, fri0 = null, fri1 = null, sat0 = null, sat1 = null, sun0 = null, sun1 = null;
                bool ck1 = false, ck2 = false, ck3 = false, ck4 = false, ck5 = false, ck6 = false, ck7 = false;
                if (sgsch.Mon != null)
                {
                    mon0 = sgsch.Mon.Split('-')[0];
                    mon1 = sgsch.Mon.Split('-')[1];
                    ck1 = true;
                }
                if (sgsch.Tue != null)
                {
                    tue0 = sgsch.Tue.Split('-')[0];
                    tue1 = sgsch.Tue.Split('-')[1];
                    ck2 = true;
                }
                if (sgsch.Wed != null)
                {
                    wed0 = sgsch.Wed.Split('-')[0];
                    wed1 = sgsch.Wed.Split('-')[1];
                    ck3 = true;
                }
                if (sgsch.Thu != null)
                {
                    thu0 = sgsch.Thu.Split('-')[0];
                    thu1 = sgsch.Thu.Split('-')[1];
                    ck4 = true;
                }
                if (sgsch.Fri != null)
                {
                    fri0 = sgsch.Fri.Split('-')[0];
                    fri1 = sgsch.Fri.Split('-')[1];
                    ck5 = true;
                }
                if (sgsch.Sat != null)
                {
                    sat0 = sgsch.Sat.Split('-')[0];
                    sat1 = sgsch.Sat.Split('-')[1];
                    ck6 = true;
                }
                if (sgsch.Sun != null)
                {
                    sun0 = sgsch.Sun.Split('-')[0];
                    sun1 = sgsch.Sun.Split('-')[1];
                    ck7 = true;
                }
                var viewModel = new AddCourseVM
                {
                    Mon1 = mon0,
                    Mon2 = mon1,
                    Tue1 = tue0,
                    Tue2 = tue1,
                    Wed1 = wed0,
                    Wed2 = wed1,
                    Thu1 = thu0,
                    Thu2 = thu1,
                    Fri1 = fri0,
                    Fri2 = fri1,
                    Sat1 = sat0,
                    Sat2 = sat1,
                    Sun1 = sun0,
                    Sun2 = sun1,
                    chk1 = ck1,
                    chk2 = ck2,
                    chk3 = ck3,
                    chk4 = ck4,
                    chk5 = ck5,
                    chk6 = ck6,
                    chk7 = ck7,
                    Instructors = ins,
                    Course = crs
                };
                return PartialView("_EditCourse", viewModel);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string getIdFaculty(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       join f in db.Faculties on d.idFaculty equals f.idFaculty
                       where i.idInstructor == tmp
                       select f.idFaculty).FirstOrDefault();
            return idf;
        }
        private string getIdDept(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       where i.idInstructor == tmp
                       select d.idDepartment).FirstOrDefault();
            return idf;
        }


        [HttpPost]
        public ActionResult Edit(AddCourseVM classes)
        {
            try
            {
                //classes.Departments = db.Departments.ToList();
                classes.Instructors = db.Instructors.Where(c => c.I_idDepartment == classes.Course.idDepartment);
                if (ModelState.IsValid)
                {
                    var dbUser = db.Courses.Single(c => c.idCourse == classes.Course.idCourse);
                    // dbUser.idCourse = classes.Course.idInstructor;
                    dbUser.CourseName = classes.Course.CourseName;
                    dbUser.SchoolYear = classes.Course.SchoolYear;
                    dbUser.StartDate = classes.Course.StartDate;
                    dbUser.EndDate = classes.Course.EndDate;
                    //dbUser.idDepartment = classes.Course.idDepartment;
                    dbUser.idInstructor = classes.Course.idInstructor;

                    db.Entry(dbUser).State = EntityState.Modified;
                    db.SaveChanges();


                    var sch = new Schedule();
                    sch.idCourse = classes.Course.idCourse;
                    sch.SchoolYear = classes.Course.SchoolYear;
                    sch.Mon = classes.Mon1 == null ? null : classes.Mon1 + "-" + classes.Mon2;
                    sch.Tue = classes.Tue1 == null ? null : classes.Tue1 + "-" + classes.Tue2;
                    sch.Wed = classes.Wed1 == null ? null : classes.Wed1 + "-" + classes.Wed2;
                    sch.Thu = classes.Thu1 == null ? null : classes.Thu1 + "-" + classes.Thu2;
                    sch.Fri = classes.Fri1 == null ? null : classes.Fri1 + "-" + classes.Fri2;
                    sch.Sat = classes.Sat1 == null ? null : classes.Sat1 + "-" + classes.Sat2;
                    sch.Sun = classes.Sun1 == null ? null : classes.Sun1 + "-" + classes.Sun2;
                    db.Entry(sch).State = EntityState.Modified;
                    db.SaveChanges();


                    return Json(new { success = true, message = "Cập nhật thành công!" });
                }
                return PartialView("_EditCourse", classes);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var crs = db.Courses.Single(c => c.idCourse == id);
            IEnumerable<DateTime> dow = ListDateOfCourse(crs.StartDate, crs.EndDate, id);
            int lth = dow.Count();
            var now = DateTime.Now;
            int[] cnt = new int[lth];
            int[] cntabs = new int[lth];
            foreach (var i in crs.STUDENT_COURSE)
            {
                int cr = 0;
                i.chksch = new List<string>(lth);
                foreach (var dt in dow)
                {
                    var tmp = db.Trackings.Where(c => c.idSC.Equals(i.idSC)).ToList();
                    if (DateTime.Compare(now.Date, dt.Date) >= 0)
                    {
                        var tt = tmp.Where(x => x.Logs.ToShortDateString() == dt.ToShortDateString()).FirstOrDefault();
                        if (tt != null)
                        {
                            if (tt.isLate != "0")
                            {
                                i.chksch.Add(tt.isLate);
                            }
                            else
                            {
                                i.chksch.Add("true");
                            }
                            cnt[cr] += 1;
                        }
                        else
                        {
                            i.chksch.Add("false");
                            cntabs[cr] += 1;
                        }
                    }
                    else
                        i.chksch.Add(null);
                    cr++;
                }
            }
            var viewModel = new CourseDetailsVM
            {
                Course = crs,
                CourseDate = dow,
                rowcntNAbs = cnt,
                rowcntAbs = cntabs,
                CourseNo = lth,
            };
            if (crs == null)
            {
                return HttpNotFound();
            }
            return View(viewModel);
        }
        //private DateTime cvt(string t)
        //{
        //    string[] a = t.Split(' ');
        //    string[] a2 = a[0].Split('/');
        //    var start = new DateTime(int.Parse(a2[2]), int.Parse(a2[1]), int.Parse((a2[0])));
        //    return start;
        //}
        private IEnumerable<DateTime> ListDateOfCourse(DateTime start, DateTime end, string idCourse)
        {
            string[] dow = new string[7];
            var lstdow = (from c in db.Courses
                          join sch in db.Schedules on c.idCourse equals sch.idCourse
                          where c.idCourse == idCourse
                          select sch).FirstOrDefault();
            dow[0] = lstdow.Mon;
            dow[1] = lstdow.Tue;
            dow[2] = lstdow.Wed;
            dow[3] = lstdow.Thu;
            dow[4] = lstdow.Fri;
            dow[5] = lstdow.Sat;
            dow[6] = lstdow.Sun;
            List<DayOfWeek> daysToChoose = new List<DayOfWeek>();
            if (dow[0] != null)
                daysToChoose.Add(DayOfWeek.Monday);
            if (dow[1] != null)
                daysToChoose.Add(DayOfWeek.Tuesday);
            if (dow[2] != null)
                daysToChoose.Add(DayOfWeek.Wednesday);
            if (dow[3] != null)
                daysToChoose.Add(DayOfWeek.Thursday);
            if (dow[4] != null)
                daysToChoose.Add(DayOfWeek.Friday);
            if (dow[5] != null)
                daysToChoose.Add(DayOfWeek.Saturday);
            if (dow[6] != null)
                daysToChoose.Add(DayOfWeek.Sunday);
            var dates = Enumerable.Range(0, (int)(end - start).TotalDays + 1)
                            .Select(d => start.AddDays(d))
                            .Where(d => daysToChoose.Contains(d.DayOfWeek));
            return dates;
        }
        private IEnumerable<DateTime> ListDateOfCourse(DateTime start, DateTime end, Schedule sch)
        {
            string[] dow = new string[7];
            dow[0] = sch.Mon;
            dow[1] = sch.Tue;
            dow[2] = sch.Wed;
            dow[3] = sch.Thu;
            dow[4] = sch.Fri;
            dow[5] = sch.Sat;
            dow[6] = sch.Sun;
            List<DayOfWeek> daysToChoose = new List<DayOfWeek>();
            if (dow[0] != null)
                daysToChoose.Add(DayOfWeek.Monday);
            if (dow[1] != null)
                daysToChoose.Add(DayOfWeek.Tuesday);
            if (dow[2] != null)
                daysToChoose.Add(DayOfWeek.Wednesday);
            if (dow[3] != null)
                daysToChoose.Add(DayOfWeek.Thursday);
            if (dow[4] != null)
                daysToChoose.Add(DayOfWeek.Friday);
            if (dow[5] != null)
                daysToChoose.Add(DayOfWeek.Saturday);
            if (dow[6] != null)
                daysToChoose.Add(DayOfWeek.Sunday);
            var dates = Enumerable.Range(0, (int)(end - start).TotalDays + 1)
                            .Select(d => start.AddDays(d))
                            .Where(d => daysToChoose.Contains(d.DayOfWeek));
            return dates;
        }
        private IEnumerable<DateTime> ListDateOfCourse(DateTime start, DateTime end, string idCourse, string idStudent)
        {
            string[] dow = new string[7];
            var lstdow = (from c in db.Courses
                          join sch in db.Schedules on c.idCourse equals sch.idCourse
                          join sc in db.STUDENT_COURSE on sch.idCourse equals sc.idCourse
                          where c.idCourse == idCourse && sc.idStudent == idStudent
                          select sch).FirstOrDefault();
            dow[0] = lstdow.Mon;
            dow[1] = lstdow.Tue;
            dow[2] = lstdow.Wed;
            dow[3] = lstdow.Thu;
            dow[4] = lstdow.Fri;
            dow[5] = lstdow.Sat;
            dow[6] = lstdow.Sun;
            List<DayOfWeek> daysToChoose = new List<DayOfWeek>();
            if (dow[0] != null)
                daysToChoose.Add(DayOfWeek.Monday);
            if (dow[1] != null)
                daysToChoose.Add(DayOfWeek.Tuesday);
            if (dow[2] != null)
                daysToChoose.Add(DayOfWeek.Wednesday);
            if (dow[3] != null)
                daysToChoose.Add(DayOfWeek.Thursday);
            if (dow[4] != null)
                daysToChoose.Add(DayOfWeek.Friday);
            if (dow[5] != null)
                daysToChoose.Add(DayOfWeek.Saturday);
            if (dow[6] != null)
                daysToChoose.Add(DayOfWeek.Sunday);
            var dates = Enumerable.Range(0, (int)(end - start).TotalDays + 1)
                            .Select(d => start.AddDays(d))
                            .Where(d => daysToChoose.Contains(d.DayOfWeek));
            return dates;
        }
        public ActionResult DetailsReport(string idCourse, string idStudent)
        {
            if (idCourse == null || idStudent == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var crs = db.Courses.Single(c => c.idCourse == idCourse);
            var std = db.Students.Single(c => c.idStudent == idStudent);
            var stc = db.STUDENT_COURSE.Single(s => s.idStudent == idStudent && s.idCourse == idCourse);
            var tmp = db.Trackings.Where(c => c.idSC.Equals(stc.idSC)).ToList();
            IEnumerable<DateTime> dow = ListDateOfCourse(crs.StartDate, crs.EndDate, idCourse, idStudent);
            var now = DateTime.Now;
            int lth = dow.Count();
            List<string> chk = new List<string>(lth);
            foreach (var dt in dow)
            {
                if (DateTime.Compare(now.Date, dt.Date) >= 0)
                {
                    var tt = tmp.Where(x => x.Logs.ToShortDateString() == dt.ToShortDateString()).FirstOrDefault();
                    if (tt != null)
                    {
                        if (tt.isLate != "0")
                        {
                            chk.Add(tt.isLate);
                        }
                        else
                        {
                            chk.Add("true");
                        }
                    }
                    else
                    {
                        chk.Add("false");
                    }
                }
                else
                    chk.Add(null);
            }
            var vm = new StdSum
            {
                Course = crs,
                idStudent = idStudent,
                S_Name = std.S_Name,
                CourseDate = dow,
                tick = chk,
                NoAbsent = stc.NoAbsent,
                NoLate = stc.NoLate,
                NoCourse = lth,
                absWarning = stc.NoAbsent >= (lth / 2) ? 1 :
                           stc.NoAbsent >= (lth / 4) && stc.NoAbsent < (lth / 2) ? 2 :
                           stc.NoAbsent >= (lth / 8) && stc.NoAbsent < (lth / 4) ? 3 : 0,
                lateWarning = stc.NoLate > 2 ? 1 : 0
            };
            return View(vm);
        }
        [HttpPost]
        public ActionResult getStd(string std)
        {
            var lstins = (from e in db.Students
                          join c in db.Classes on e.idClass equals c.idClass
                          where e.idStudent == std || e.S_Name == std
                          select new
                          {
                              e.idStudent,
                              e.S_Name,
                              e.S_Phone,
                              e.S_Email,
                              e.S_Dob,
                              c.ClassName
                          }).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstins);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getStdByClass(string cls)
        {
            var lstins = (from e in db.Students
                          join c in db.Classes on e.idClass equals c.idClass
                          where c.idClass==cls
                          select new
                          {
                              e.idStudent,
                              e.S_Name,
                              e.S_Dob,
                              c.ClassName
                          }).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstins);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getClassList()
        {
            var lstins = (from c in db.Classes
                          select new
                          {
                              c.idClass,
                              c.ClassName
                          }).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstins);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddStudentToCourse(string id)
        {
            var cr = db.Courses.Single(c => c.idCourse == id);
            var vm = new STUDENT_COURSE
            {
                idCourse = cr.idCourse,
                SchoolYear = cr.SchoolYear
            };
            return PartialView("_CreateStudent", vm);
        }

        [HttpPost]
        public ActionResult AddStudentToCourse([Bind(Exclude = "idSC")]STUDENT_COURSE facu)
        {
            bool facultyExists = db.STUDENT_COURSE.Any(fa => fa.idStudent.Equals(facu.idStudent) && fa.idCourse.Equals(facu.idCourse));
            if (facultyExists)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = "Đã tồn tại mã sinh viên", Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };
            }
            else
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var stc = new STUDENT_COURSE();
                        stc.idStudent = facu.idStudent;
                        stc.idCourse = facu.idCourse;
                        stc.SchoolYear = facu.SchoolYear;
                        stc.idSC = 0;
                        stc.NoAbsent = 0;
                        stc.NoLate = 0;

                        db.STUDENT_COURSE.Add(stc);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateStudent", facu);
                }
                catch (Exception ex)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = ex.Message, Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
            }
        }
        public ActionResult PartialSta(string parameterID,string idc, string sch)
        {
            if (parameterID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var std = (from s in db.Students
                       join c in db.Classes on s.idClass equals c.idClass
                       where c.idClass == parameterID
                       select new AddStdVM
                       {
                           idStudent = s.idStudent,
                           S_Name = s.S_Name,
                           S_Dob = s.S_Dob,
                           ClassName = c.ClassName,
                           idCourse = idc,
                           SchoolYear = sch,
                           Selected = false
                       }).ToList();
            var viewModel = new List<AddStdVM>(std);
            if (std == null)
            {
                return HttpNotFound();
            }
            return PartialView("_PartialListStd", viewModel);
        }
        public ActionResult AddListStudentToCourse(string id)
        {
            var cr = db.Courses.Single(c => c.idCourse == id);
            var vm = new AddStdVM
            {
                idCourse = cr.idCourse,
                SchoolYear = cr.SchoolYear
            };
            return PartialView("_CreateListStudent", vm);
        }

        [HttpPost]
        public ActionResult AddListStudentToCourse([Bind(Exclude = "idSC")]List<AddStdVM> facu)
        {
            var lstd = new List<AddStdVM>();
            foreach (var item in facu)
            {
                if (item.Selected)
                {
                    var std = new AddStdVM();
                    std.idStudent = item.idStudent;
                    std.idCourse = item.idCourse;
                    std.SchoolYear = item.SchoolYear;
                    std.idSC = 0;
                    std.NoAbsent = 0;
                    std.NoLate = 0;
                    lstd.Add(std);
                }
            }
            try
            {
                if (ModelState.IsValid)
                {
                    List<STUDENT_COURSE> list = new List<STUDENT_COURSE>();
                    foreach (var a in lstd)
                    {
                        bool facultyExists = db.STUDENT_COURSE.Any(fa => fa.idStudent.Equals(a.idStudent) && fa.idCourse.Equals(a.idCourse));
                        if (facultyExists)
                        {
                            //return new JsonResult
                            //{
                            //    Data = new { ErrorMessage = "Đã tồn tại mã sinh viên" + a.idStudent, Success = false },
                            //    ContentEncoding = System.Text.Encoding.UTF8,
                            //    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                            //};
                            continue;
                        }
                        else
                        {
                            var stc = new STUDENT_COURSE();
                            stc.idStudent = a.idStudent;
                            stc.idCourse = a.idCourse;
                            stc.SchoolYear = a.SchoolYear;
                            stc.idSC = 0;
                            stc.NoAbsent = 0;
                            stc.NoLate = 0;
                            list.Add(stc);
                        }
                    }
                    db.STUDENT_COURSE.AddRange(list);
                    db.SaveChanges();
                    return Json(new { success = true, message = "Thêm mới thành công!" });
                }
                return PartialView("_CreateListStudent", facu);
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = ex.Message, Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };

            }
        }
        public FileResult DownloadExcel()
        {
            string path = "/Doc/MauExcelLopTinChi.xlsx";
            return File(path, "application/vnd.ms-excel", "MauExcelLopTinChi.xlsx");
        }
        public ActionResult AddListStudentToCourseFromExcel()
        {
            //DocumentModelView dc = new DocumentModelView();
            return PartialView("_CreateListStudentFromExcel");
        }
        [HttpPost]
        public ActionResult AddListStudentToCourseFromExcel(HttpPostedFileBase files)
        {
            List<string> data = new List<string>();
            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0)
                {
                    foreach (string file in Request.Files)
                    {
                        var _file = Request.Files[file];
                        if (_file != null)
                        {
                            //if (_file.ContentType == "application/vnd.ms-excel" || _file.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                            //{


                            string filename = _file.FileName;
                            string targetpath = Server.MapPath("~/Uploads/");
                            _file.SaveAs(targetpath + filename);
                            string pathToExcelFile = targetpath + filename;
                            var connectionString = "";
                            //if (filename.EndsWith(".xls"))
                            //{
                            //    connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data source={0}; Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";", pathToExcelFile);
                            //}
                            //else if (filename.EndsWith(".xlsx"))
                            //{
                            connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                            //}

                            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                            var ds = new DataSet();

                            adapter.Fill(ds, "ExcelTable");

                            DataTable dtable = ds.Tables["ExcelTable"];

                            string sheetName = "Sheet1";

                            var excelFile = new ExcelQueryFactory(pathToExcelFile);
                            excelFile.AddMapping<STUDENT_COURSE>(c => c.idStudent, "Mã sinh viên");
                            excelFile.AddMapping<STUDENT_COURSE>(c => c.idCourse, "Mã lớp tín chỉ đăng ký");
                            excelFile.AddMapping<STUDENT_COURSE>(c => c.SchoolYear, "Năm học");

                            var artistAlbums = from a in excelFile.Worksheet<STUDENT_COURSE>(sheetName) select a;

                            foreach (var a in artistAlbums)
                            {
                                try
                                {
                                    if ((a.idStudent != "" && a.idCourse != "" && a.SchoolYear != "") || (a.idStudent != null && a.idCourse != null && a.SchoolYear != null))
                                    {
                                        if (db.STUDENT_COURSE.Any(c => c.idCourse == a.idCourse && c.idStudent == a.idStudent && c.SchoolYear == c.SchoolYear))
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            STUDENT_COURSE TU = new STUDENT_COURSE();
                                            TU.idStudent = a.idStudent;
                                            TU.idCourse = a.idCourse;
                                            TU.SchoolYear = a.SchoolYear;
                                            TU.NoLate = 0;
                                            TU.idSC = 0;
                                            TU.NoAbsent = 0;
                                            db.STUDENT_COURSE.Add(TU);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        if (a.idStudent == "" || a.idStudent == null || a.idCourse == "" || a.idCourse == null || a.SchoolYear == "" || a.SchoolYear == null)
                                            continue;
                                    }
                                }

                                catch (DbEntityValidationException ex)
                                {
                                    return new JsonResult
                                    {
                                        Data = new { ErrorMessage = "Nhập từ file thất bại, vui lòng kiểm tra lại nội dung và định dạng file Excel", Success = false },
                                        ContentEncoding = System.Text.Encoding.UTF8,
                                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                                    };
                                }
                            }
                            //deleting excel file from folder  
                            if ((System.IO.File.Exists(pathToExcelFile)))
                            {
                                System.IO.File.Delete(pathToExcelFile);
                            }
                            return Json(new { success = true, message = "Nhập dữ liệu từ Excel thành công!" });
                        }
                        else
                        {
                            return new JsonResult
                            {
                                Data = new { ErrorMessage = "Vui lòng chọn file Excel", Success = false },
                                ContentEncoding = System.Text.Encoding.UTF8,
                                JsonRequestBehavior = JsonRequestBehavior.DenyGet
                            };
                        }
                    }

                }
            }
            return PartialView("_CreateListStudentFromExcel");
        }
    }
}