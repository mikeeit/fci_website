﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq.Dynamic;
using FaceCheckin.Models.ViewModels;
using FaceCheckin.Security;

namespace FaceCheckin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class FacultyController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult getFaculties()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idFaculty = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var FacultyName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            var facu = db.Faculties.ToList();
            //SEARCHING...
            if (!string.IsNullOrEmpty(idFaculty))
            {
                facu = facu.Where(a => a.idFaculty.Contains(idFaculty)).ToList();
            }
            if (!string.IsNullOrEmpty(FacultyName))
            {
                facu = facu.Where(a => a.FacultyName.Contains(FacultyName)).ToList();
            }

            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                facu = facu.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }

            TotalRecords = facu.ToList().Count();
            var NewItems = facu.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            return PartialView("_CreateFaculty");
        }

        [HttpPost]
        public ActionResult Create(Faculty facu)
        {
            bool facultyExists = db.Faculties.Any(fa => fa.idFaculty.Equals(facu.idFaculty));

            if (facultyExists)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = "Đã tồn tại Khoa", Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };
            }
            else
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.Faculties.Add(facu);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateFaculty", facu);
                }
                catch (Exception ex)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = ex.Message, Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };

                }
            }

        }

        public ActionResult Edit(string id)
        {
            try
            {
                if (id == "")
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Faculty facu = db.Faculties.Find(id);
                if (facu == null)
                    return HttpNotFound();
                return PartialView("_EditFaculty", facu);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult getFacuDetails(string id)
        {
            try
            {
                var rslt = (from e in db.Faculties
                            where e.idFaculty == id.ToString()
                            select new
                            {
                                FacultyName = e.FacultyName
                            });
                return Json(rslt, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult deleteConfirmed(string id)
        {
            try
            {
                var rslt = db.Faculties.FirstOrDefault(x => x.idFaculty == id.ToString());
                if (rslt != null)
                {
                    db.Faculties.Remove(rslt);
                    db.SaveChanges();
                    return Json("Xóa thành công!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        [HttpPost]
        public ActionResult Edit(Faculty facu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(facu).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(new { success = true, message = "Cập nhật thành công!" });
                }
                return PartialView("_EditFaculty", facu);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult getDetails(string id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idDepartment = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var DepartmentName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;

            var crs = (from c in db.Faculties
                       join st in db.Departments on c.idFaculty equals st.idFaculty
                       where st.idFaculty == id
                       select new
                       {
                           idDepartment = st.idDepartment,
                           DepartmentName = st.DepartmentName
                       }).ToList();
            if (!string.IsNullOrEmpty(idDepartment))
            {
                crs = crs.Where(a => a.idDepartment.Contains(idDepartment)).ToList();
            }
            if (!string.IsNullOrEmpty(DepartmentName))
            {
                crs = crs.Where(a => a.DepartmentName.Contains(DepartmentName)).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                crs = crs.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }
            TotalRecords = crs.ToList().Count();
            var NewItems = crs.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(string id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Faculty faculty = db.Faculties.Find(id);
            //if (faculty == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(faculty);
            var faculty = db.Faculties.Single(c => c.idFaculty == id);
            return View(faculty);
        }
    }
}