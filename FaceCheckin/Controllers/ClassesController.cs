﻿using FaceCheckin.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using System.Net;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq.Dynamic;
using FaceCheckin.Models.ViewModels;
using FaceCheckin.Security;
using System.Web.Script.Serialization;
using LinqToExcel;
using System.Data.OleDb;
using System.Data;
using System.Data.Entity.Validation;

namespace FaceCheckin.Controllers
{
    [CustomAuthorize(Roles = "Admin,Trưởng khoa,Phó khoa,Trưởng bộ môn,Phó bộ môn,Giảng viên,Trợ giảng")]
    public class ClassesController : Controller
    {
        private FCI_DBEntities db = new FCI_DBEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            var facu = CustomFacu();
            ViewBag.FacuList = facu;
            var schy = db.Classes.ToList().GroupBy(test => test.SchoolYear).Select(grp => grp.First());
            ViewBag.SchList = schy;
            return View();
        }
        public ActionResult Summary(string id)
        {
            //Session["idstd"] = id;
            var schy = db.Courses.ToList().GroupBy(test => test.SchoolYear).Select(grp => grp.First());
            ViewBag.SchList = schy;
            var std = db.Students.Single(c => c.idStudent == id);
            return View(std);
        }
        [HttpPost]
        public ActionResult getSum(string id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idCourse = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var SchoolYear = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
            //var idStudent = Url.RequestContext.RouteData.Values["id"].ToString();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            //var ids=TempData["idstd"].ToString();
            var crs = (from c in db.Courses
                       join st in db.STUDENT_COURSE on c.idCourse equals st.idCourse
                       where st.idStudent == id
                       select new
                       {
                           idCourse = c.idCourse,
                           CourseName = c.CourseName,
                           SchoolYear = c.SchoolYear,
                           AbsSum = st.NoAbsent + "/" + c.NumberOfCourse,
                           LateSum = st.NoLate + "/" + (c.NumberOfCourse - st.NoAbsent)
                       }).ToList();
            if (!string.IsNullOrEmpty(idCourse))
            {
                crs = crs.Where(a => a.idCourse.Contains(idCourse)).ToList();
            }
            if (!string.IsNullOrEmpty(SchoolYear))
            {
                crs = crs.Where(a => a.SchoolYear == SchoolYear).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                crs = crs.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }
            TotalRecords = crs.ToList().Count();
            var NewItems = crs.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        private List<Faculty> CustomFacu()
        {
            var facu = db.Faculties.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                facu = (from f in db.Faculties
                        join d in db.Departments on f.idFaculty equals d.idFaculty
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select f).ToList();
            }
            return facu;
        }

        [HttpPost]
        public ActionResult getDeptList(string id)
        {
            var lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            if (!User.IsInRole("Admin") && !User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                lstdept = (from e in db.Departments
                           where e.idFaculty == id.ToString() && e.idDepartment == idf.ToString()
                           select new
                           {
                               e.idDepartment,
                               e.DepartmentName
                           }).ToList();
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstdept);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getInsList(string id)
        {
            var lstins = (from e in db.Instructors
                          where e.I_idDepartment == id.ToString()
                          select new
                          {
                              e.idInstructor,
                              e.I_Name
                          }).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(lstins);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult getClasses()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idClass = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var ClassName = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
            var InstructorName = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
            var DepartmentName = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            var SchoolYear = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;
            var classes = (from e in db.Classes
                           join ab in db.Instructors on e.idInstructor equals ab.idInstructor into a
                           from ab in a.DefaultIfEmpty()
                           join f in db.Departments on e.idDepartment equals f.idDepartment
                           join i in db.Instructors on ab.idInstructor equals i.idInstructor into ic
                           from i in ic.DefaultIfEmpty()
                           select new
                           {
                               idClass = e.idClass,
                               ClassName = e.ClassName,
                               DepartmentName = f.DepartmentName,
                               SchoolYear = e.SchoolYear,
                               InstructorName = ab == null ? "(Chưa có GV cố vấn)" : ab.I_Name
                           }).ToList();
            if (User.IsInRole("Giảng viên") || User.IsInRole("Trợ giảng"))
            {
                string tmp = Session["idIns"].ToString();
                classes = (from e in db.Classes
                           join ab in db.Instructors on e.idInstructor equals ab.idInstructor into a
                           from ab in a.DefaultIfEmpty()
                           join f in db.Departments on e.idDepartment equals f.idDepartment
                           join i in db.Instructors on ab.idInstructor equals i.idInstructor into ic
                           from i in ic.DefaultIfEmpty()
                           where e.idInstructor == tmp
                           select new
                           {
                               idClass = e.idClass,
                               ClassName = e.ClassName,
                               DepartmentName = f.DepartmentName,
                               SchoolYear = e.SchoolYear,
                               InstructorName = ab.I_Name
                           }).ToList();
            }
            //SEARCHING...
            if (!string.IsNullOrEmpty(idClass))
            {
                classes = classes.Where(a => a.idClass.Contains(idClass)).ToList();
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                classes = classes.Where(a => a.ClassName.Contains(ClassName)).ToList();
            }
            if (!string.IsNullOrEmpty(DepartmentName))
            {
                classes = classes.Where(a => a.DepartmentName.ToString() == DepartmentName.ToString()).ToList();
            }
            if (!string.IsNullOrEmpty(InstructorName))
            {
                classes = classes.Where(a => a.InstructorName.Contains(InstructorName)).ToList();
            }
            if (!string.IsNullOrEmpty(SchoolYear))
            {
                classes = classes.Where(a => a.SchoolYear == SchoolYear).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                classes = classes.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }

            TotalRecords = classes.ToList().Count();
            var NewItems = classes.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        private ClassesVM CustomDept()
        {
            var dept = db.Departments.ToList();
            if (!User.IsInRole("Admin"))
            {
                string tmp = Session["idIns"].ToString();
                var idf = getIdDept(tmp);
                dept = (from d in db.Departments
                        join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                        where i.idInstructor == tmp
                        select d).ToList();
                if (!User.IsInRole("Trưởng khoa") && !User.IsInRole("Phó khoa"))
                {
                    dept = (from e in db.Departments
                            where e.idDepartment == idf.ToString()
                            select e).ToList();
                }
            }
            var viewModel = new ClassesVM
            {
                Departments = dept
            };
            return viewModel;
        }
        [CustomAuthorize(Roles = "Admin,Trưởng khoa,Trưởng bộ môn")]
        public ActionResult Create(string parameterID)
        {
            //var facu = CustomFacu();
            //ViewBag.FacuList = facu;
            //var viewModel = CustomDept();
            var deptname = db.Departments.Single(c => c.idDepartment == parameterID).DepartmentName;
            var viewModel = new Class
            {
                idDepartment = parameterID,
                DepartmentName = deptname
            };
            return PartialView("_CreateClass", viewModel);
        }
        [CustomAuthorize(Roles = "Admin,Trưởng khoa,Trưởng bộ môn")]
        [HttpPost]
        public ActionResult Create(Class classes)
        {
            try
            {
                bool classExists = db.Classes.Any(fa => fa.idClass.Equals(classes.idClass));

                if (classExists)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = "Đã tồn tại Lớp", Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        db.Classes.Add(classes);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateClass", classes);
                }
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = ex.Message, Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };

            }
        }
        public ActionResult getClassDetails(string id)
        {
            try
            {
                var rslt = (from e in db.Classes
                            join ab in db.Instructors on e.idInstructor equals ab.idInstructor into a
                            from ab in a.DefaultIfEmpty()
                            join f in db.Departments on e.idDepartment equals f.idDepartment
                            join i in db.Instructors on ab.idInstructor equals i.idInstructor into ic
                            from i in ic.DefaultIfEmpty()
                            where e.idClass == id.ToString()
                            select new
                            {
                                idClass = e.idClass,
                                ClassName = e.ClassName,
                                DepartmentName = f.DepartmentName,
                                InstructorName = ab == null ? "(Chưa có GV cố vấn)" : ab.I_Name
                            }).ToList();
                //var rslt = db.Faculties.Where(f => f.idFaculty.Equals(id));
                return Json(rslt, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult deleteConfirmed(string id)
        {
            try
            {
                var rslt = db.Classes.FirstOrDefault(x => x.idClass == id.ToString());
                if (rslt != null)
                {
                    db.Classes.Remove(rslt);
                    db.SaveChanges();
                    return Json("Xóa thành công!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult Edit(string id)
        {
            try
            {
                if (id == "")
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var classes = db.Classes.Single(c => c.idClass == id);
                if (classes == null)
                    return HttpNotFound();
                var ins = db.Instructors.Where(c => c.I_idDepartment == classes.idDepartment);
                var viewModel = new ClassesVM
                {
                    Class = classes,
                    Instructors = ins
                };
                return PartialView("_EditClass", viewModel);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private string getIdFaculty(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       join f in db.Faculties on d.idFaculty equals f.idFaculty
                       where i.idInstructor == tmp
                       select f.idFaculty).FirstOrDefault();
            return idf;
        }
        private string getIdDept(string tmp)
        {
            var idf = (from d in db.Departments
                       join i in db.Instructors on d.idDepartment equals i.I_idDepartment
                       where i.idInstructor == tmp
                       select d.idDepartment).FirstOrDefault();
            return idf;
        }

        [HttpPost]
        public ActionResult Edit(ClassesVM classes)
        {
            try
            {
                classes.Instructors = db.Instructors.Where(c => c.I_idDepartment == classes.Class.idDepartment);
                if (ModelState.IsValid)
                {
                    var dbUser = db.Classes.Single(c => c.idClass == classes.Class.idClass);
                    dbUser.ClassName = classes.Class.ClassName;
                    dbUser.SchoolYear = classes.Class.SchoolYear;
                    //dbUser.idDepartment = classes.Course.idDepartment;
                    dbUser.idInstructor = classes.Class.idInstructor;

                    db.Entry(dbUser).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(new { success = true, message = "Cập nhật thành công!" });
                }
                return PartialView("_EditClass", classes);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult getDetails(string id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            var Draw = Request.Form.GetValues("draw").FirstOrDefault();
            var Start = Request.Form.GetValues("start").FirstOrDefault();
            var Length = Request.Form.GetValues("length").FirstOrDefault();

            var SortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][data]").FirstOrDefault();
            var SortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

            var idStudent = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
            var S_Name = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();

            int PageSize = Length != null ? Convert.ToInt32(Length) : 0;
            int Skip = Start != null ? Convert.ToInt32(Start) : 0;
            int TotalRecords = 0;

            var crs = (from c in db.Classes
                       join st in db.Students on c.idClass equals st.idClass
                       where st.idClass == id
                       select new
                       {
                           idStudent = st.idStudent,
                           S_Name = st.S_Name,
                           S_Dob = st.S_Dob,
                           S_Email = st.S_Email,
                           S_Phone = st.S_Phone
                       }).ToList();
            if (!string.IsNullOrEmpty(idStudent))
            {
                crs = crs.Where(a => a.idStudent.Contains(idStudent)).ToList();
            }
            if (!string.IsNullOrEmpty(S_Name))
            {
                crs = crs.Where(a => a.S_Name.Contains(S_Name)).ToList();
            }
            if (!(string.IsNullOrEmpty(SortColumn) && string.IsNullOrEmpty(SortColumnDir)))
            {
                crs = crs.OrderBy(SortColumn + " " + SortColumnDir).ToList();
            }
            TotalRecords = crs.ToList().Count();
            var NewItems = crs.Skip(Skip).Take(PageSize == -1 ? TotalRecords : PageSize).ToList();

            return Json(new { draw = Draw, recordsFiltered = TotalRecords, recordsTotal = TotalRecords, data = NewItems }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(string id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Class classes = db.Classes.Find(id);
            //if (classes == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(classes);
            var classes = db.Classes.Single(c => c.idClass == id);
            return View(classes);
        }
        public ActionResult CreateNewStudent(string parameterID)
        {
            var cls = db.Classes.Single(c => c.idClass == parameterID).ClassName;
            var viewModel = new Student
            {
                idClass = parameterID,
                ClassName = cls
            };
            return PartialView("_CreateStudent", viewModel);
        }

        [HttpPost]
        public ActionResult CreateNewStudent(Student instr)
        {
            try
            {
                var dept = db.Classes.ToList();
                instr.Classes = dept;
                bool stdExists = db.Students.Any(fa => fa.idStudent.Equals(instr.idStudent));
                if (stdExists)
                {
                    return new JsonResult
                    {
                        Data = new { ErrorMessage = "Đã tồn tại Sinh viên", Success = false },
                        ContentEncoding = System.Text.Encoding.UTF8,
                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                    };
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        db.Students.Add(instr);
                        db.SaveChanges();
                        return Json(new { success = true, message = "Thêm mới thành công!" });
                    }
                    return PartialView("_CreateStudent", instr);
                }
            }
            catch (Exception ex)
            {
                return new JsonResult
                {
                    Data = new { ErrorMessage = ex.Message, Success = false },
                    ContentEncoding = System.Text.Encoding.UTF8,
                    JsonRequestBehavior = JsonRequestBehavior.DenyGet
                };

            }
        }
        public FileResult DownloadExcel()
        {
            string path = "/Doc/MauExcelLopSinhHoat.xlsx";
            return File(path, "application/vnd.ms-excel", "MauExcelLopSinhHoat.xlsx");
        }
        public ActionResult AddListStudentToClassFromExcel()
        {
            return PartialView("_CreateListStudentFromExcel");
        }
        [HttpPost]
        public ActionResult AddListStudentToClassFromExcel(HttpPostedFileBase files)
        {
            List<string> data = new List<string>();
            if (ModelState.IsValid)
            {
                if (Request.Files.Count > 0)
                {
                    foreach (string file in Request.Files)
                    {
                        var _file = Request.Files[file];
                        if (_file != null)
                        {
                            string filename = _file.FileName;
                            string targetpath = Server.MapPath("~/Uploads/");
                            _file.SaveAs(targetpath + filename);
                            string pathToExcelFile = targetpath + filename;
                            var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);

                            var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                            var ds = new DataSet();

                            adapter.Fill(ds, "ExcelTable");

                            DataTable dtable = ds.Tables["ExcelTable"];

                            string sheetName = "Sheet1";

                            var excelFile = new ExcelQueryFactory(pathToExcelFile);
                            excelFile.AddMapping<Student>(c => c.idStudent, "Mã sinh viên");
                            excelFile.AddMapping<Student>(c => c.S_Name, "Họ tên sinh viên");
                            excelFile.AddMapping<Student>(c => c.S_Dob, "Ngày sinh");
                            excelFile.AddMapping<Student>(c => c.S_Email, "Email");
                            excelFile.AddMapping<Student>(c => c.S_Phone, "Số điện thoại");
                            excelFile.AddMapping<Student>(c => c.idClass, "Lớp sinh hoạt");

                            var artistAlbums = from a in excelFile.Worksheet<Student>(sheetName) select a;

                            foreach (var a in artistAlbums)
                            {
                                try
                                {
                                    if ((a.idStudent != "" && a.S_Name != "" && a.idClass != "") || (a.idStudent != null && a.S_Name != null && a.idClass != null))
                                    {
                                        if (db.Students.Any(c => c.idStudent == a.idStudent && c.idClass == a.idClass))
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            Student TU = new Student();
                                            TU.idStudent = a.idStudent;
                                            TU.S_Name = a.S_Name;
                                            TU.S_Dob = a.S_Dob;
                                            TU.S_Email = a.S_Email == "" ? null : a.S_Email;
                                            TU.S_Phone = a.S_Phone == "" ? null : a.S_Phone;
                                            TU.idClass = a.idClass;
                                            db.Students.Add(TU);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        if (a.idStudent == "" || a.idStudent == null || a.S_Name == "" || a.S_Name == null || a.idClass == "" || a.idClass == null)
                                            continue;
                                    }
                                }

                                catch (DbEntityValidationException ex)
                                {
                                    return new JsonResult
                                    {
                                        Data = new { ErrorMessage = "Nhập từ file thất bại, vui lòng kiểm tra lại nội dung và định dạng file Excel", Success = false },
                                        ContentEncoding = System.Text.Encoding.UTF8,
                                        JsonRequestBehavior = JsonRequestBehavior.DenyGet
                                    };
                                }
                            }
                            //deleting excel file from folder  
                            if ((System.IO.File.Exists(pathToExcelFile)))
                            {
                                System.IO.File.Delete(pathToExcelFile);
                            }
                            return Json(new { success = true, message = "Nhập dữ liệu từ Excel thành công!" });
                        }
                        else
                        {
                            return new JsonResult
                            {
                                Data = new { ErrorMessage = "Vui lòng chọn file Excel", Success = false },
                                ContentEncoding = System.Text.Encoding.UTF8,
                                JsonRequestBehavior = JsonRequestBehavior.DenyGet
                            };
                        }
                    }

                }
            }
            return PartialView("_CreateListStudentFromExcel");
        }
    }
}