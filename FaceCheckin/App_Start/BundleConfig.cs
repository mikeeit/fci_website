﻿using System.Web;
using System.Web.Optimization;

namespace FaceCheckin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jQuery/jquery-{version}.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/datepicker3.css",
                      "~/Content/bootstrap.css",
                      "~/Content/toastr.css",
                      "~/Content/DataTables/css/dataTables.bootstrap.css",
                      "~/Content/DataTables/css/buttons.dataTables.css",
                      "~/Content/DataTables/css/buttons.bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new ScriptBundle("~/bundles/DataTables").Include(
                       "~/Scripts/DataTables/jquery.dataTables.js",
                       "~/Scripts/DataTables/dataTables.jqueryui.js",
                       "~/Scripts/DataTables/dataTables.bootstrap.js",
                       "~/Scripts/DataTables/dataTables.buttons.js",
                       "~/Scripts/jszip.js",
                       "~/Scripts/DataTables/buttons.pdfmake.js",
                       "~/Scripts/DataTables/buttons.html5.js",
                       "~/Scripts/DataTables/buttons.flash.js",
                       "~/Scripts/DataTables/buttons.print.js",
                       "~/Scripts/DataTables/jquery.dataTables.columnFilter.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                       "~/Scripts/bootstrap.js",
                       "~/Scripts/datepicker/bootstrap-datepicker.js",
                       "~/Scripts/toastr.js",
                       "~/Scripts/bootbox.js",
                       "~/Scripts/respond.js"));
        }
    }
}
